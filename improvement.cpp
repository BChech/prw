#include "improvement.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS ZWIĄZANY Z POPRAWĄ WYNIKÓW KALIBRACJI
//=====================================================================================

//konstruktor klasy
Improvement::Improvement(Gui gui, ImageLoader images,Configuration conf)
{
    change_end=conf.keys[18];
    next_cross=conf.keys[10];
    pick_image_key=conf.keys[7];
    delete_key=conf.keys[11];

    tv_improvement=new ImageImprovement(images.tv,gui.tv_interface->points,conf.keys,Scalar(0,255,0),Scalar(0,0,255),conf.parameters);
    ir_improvement=new ImageImprovement(images.ir,gui.tv_interface->points,conf.keys,Scalar(0,255,0),Scalar(0,0,255),conf.parameters);

    tv_points=gui.tv_interface->points;
    ir_points=gui.ir_interface->points;

    length=conf.parameters[3];
    thickness=conf.parameters[4];

    tv_improvement->update_crosses((tv_points),tv_akt,0);
    ir_improvement->update_crosses((ir_points),ir_akt,0);

    initialize_canvas(images.tv.cols,images.tv.rows);
}

//funkcja umożlwiwiająca wybór aktywnego punktu oraz zmianę jego położenia
void Improvement::improve()
{
    int size=tv_points.size();

    char key;
    while(1)
    {
        if(key==delete_key)
        {
            if(tv_points.size()>3&&ir_points.size()>3)
            {
                if(pick_image_value==1)
                {
                    tv_points.erase(tv_points.begin() + tv_akt);
                    ir_points.erase(ir_points.begin() + tv_akt);
                }

                if(pick_image_value==-1)
                {
                    tv_points.erase(tv_points.begin() + ir_akt);
                    ir_points.erase(ir_points.begin() + ir_akt);
                }

                size=tv_points.size();
                if(tv_akt==size)tv_akt=0;
                if(ir_akt==size)ir_akt=0;
                tv_improvement->update_crosses((tv_points),tv_akt,key);
                ir_improvement->update_crosses((ir_points),ir_akt,key);
            }
        }

        if(key==pick_image_key)
            pick_image_value=-pick_image_value;

        if(pick_image_value==1)
            tv_improvement->update_crosses((tv_points),tv_akt,key);
        if(pick_image_value==-1)
            ir_improvement->update_crosses((ir_points),ir_akt,key);

        update_canvas(tv_improvement->image_processed,ir_improvement->image_processed,pick_image_value);

        key=waitKey();

        if(key==next_cross)
        {
            if(pick_image_value==1)
            {
                tv_akt=tv_akt+1;
                if(tv_akt>=size)tv_akt=0;
            }
            if(pick_image_value==-1)
            {
                ir_akt=ir_akt+1;
                if(ir_akt>=size)ir_akt=0;
            }
        }

        if(key==change_end){cout<<"Zakonczono poprawe"<<endl;break;}
    }
}
