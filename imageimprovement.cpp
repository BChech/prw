#include "imageimprovement.h"

//=======================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS POPRAWY WYNIKÓW KALIBRACJI ZWIĄZANY Z JEDNYM OBRAZEM
//=======================================================================================

//konstruktor klasy
ImageImprovement::ImageImprovement(Mat image, vector<Point> saved_points, char *keys, Scalar active_color, Scalar written_color, float *parameters)
{
    up=keys[0];left=keys[1];down=keys[2];right=keys[3];
    zoom_in=keys[4];zoom_out=keys[5];save=keys[6];
    cross_color=active_color;crosses_written_color=written_color;
    rect_resize=parameters[0];speed_initial=parameters[1];speed_reduction=parameters[2];
    speed=speed_initial;
    width=image.cols;height=image.rows;
    rect_width=width;rect_height=height;
    rect_middle=Point(width/2,height/2);
    image.copyTo(image_original);
    image.copyTo(image_original_with_cross);
    length=parameters[3];
    thickness=parameters[4];
    image_original_with_cross.copyTo(image_processed);
    rect=Rect(rect_corner.x,rect_corner.y,rect_width,rect_height);
}

//funkcja uaktualniająca wektor zapisanych punuktów na podstawie aktualnie wciśniętego klawisza kalwiatury
void ImageImprovement::update_crosses(vector<Point> &saved_points,int akt,char key)
{
    update_point((saved_points[akt]),key);

    int size=saved_points.size();

    image_original.copyTo(image_original_with_cross);

    for(int i=0;i<size;i++)
    {
        draw_cross(image_original_with_cross,saved_points[i],crosses_written_color);
    }
        draw_cross(image_original_with_cross,saved_points[akt],cross_color);

    rect=Rect(rect_corner.x,rect_corner.y,rect_width,rect_height);
    image_processed=image_original_with_cross(rect);
    resize(image_processed,image_processed,Size(width,height),0,0,CV_INTER_NN);
}
