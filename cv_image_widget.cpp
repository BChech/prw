#include <cv_image_widget.h>

CVImageWidget::CVImageWidget( QWidget *parent ) : QWidget(parent)
{
}

QSize CVImageWidget::sizeHint() const { return _qimage.size(); }

QSize CVImageWidget::minimumSizeHint() const { return _qimage.size(); }

void CVImageWidget::showImage( const cv::Mat& image, bool fixed_shape )
{
    // Resize the image
    cv::Mat canvas;
    int widget_height = this->geometry().height();
    int widget_width = this->geometry().width();
    cv::Size new_sz( widget_width, widget_height );
    if( fixed_shape ){
        float im_ratio = float(image.cols)/image.rows;
        float widget_ratio = float(widget_width)/widget_height;
        if( im_ratio < widget_ratio ){
            new_sz.height = widget_height;
            new_sz.width = image.cols * widget_height / image.rows;
        }else{
            new_sz.width = widget_width;
            new_sz.height = image.rows * widget_width / image.cols;
        }
        canvas = cv::Mat::zeros( widget_height, widget_width, CV_8UC3 );
        cv::Mat roi = canvas( cv::Rect( (widget_width-new_sz.width)/2,
                                        (widget_height-new_sz.height)/2,
                                        new_sz.width, new_sz.height ) );
        cv::resize( image, roi, new_sz );
    }else{
        cv::resize( image, canvas, new_sz );
    }


    // Convert the image to the RGB888 format
    switch (image.type()) {
    case CV_8UC1:
        cvtColor( canvas, _input_image, CV_GRAY2RGB );
        break;
    case CV_8UC3:
        cvtColor( canvas, _input_image, CV_BGR2RGB );
        break;
    }

    repaint();
}

void CVImageWidget::paintEvent( QPaintEvent* /*event*/ )
{
    if(_input_image.empty()) return;
    cv::Mat _tmp;
    _input_image.copyTo(_tmp);

    // QImage needs the data to be stored continuously in memory
    //assert(_tmp.isContinuous());
    if( !_tmp.isContinuous() ) return;
    // Assign OpenCV's image buffer to the QImage. Note that the bytesPerLine parameter
    // (http://qt-project.org/doc/qt-4.8/qimage.html#QImage-6) is 3*width because each pixel
    // has three bytes.
    _qimage = QImage(_tmp.data, _tmp.cols, _tmp.rows, _tmp.cols*3, QImage::Format_RGB888);

    // Display the image
    QPainter painter(this);
    painter.drawImage(QPoint(0,0), _qimage);
    painter.end();
}

void CVImageWidget::mousePressEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        emit signal_mouseLeftPress(event);
    }
    else if(event->button() == Qt::RightButton)
    {
        emit signal_mouseRightPress(event);
    }
}

void CVImageWidget::mouseMoveEvent(QMouseEvent *event)
{
    emit signal_mouseLeftMove(event);
}

void CVImageWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        emit signal_mouseLeftRelease(event);
    }
    else if(event->button() == Qt::RightButton)
    {
        emit signal_mouseRightRelease(event);
    }
}
