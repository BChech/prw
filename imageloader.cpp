#include "imageloader.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA WCZYTANIE OBRAZÓW
//=====================================================================================

//konstruktor klasy
ImageLoader::ImageLoader(Configuration conf)
{
    if(conf.tcp_ip==0)
    {
        tv_path=conf.tv_path;
        ir_path=conf.ir_path;
    }

    if(conf.tcp_ip==1)
    {
        tcp_host_name=conf.tcp_host_name;
        tcp_port_number=conf.tcp_port_number;
    }

    tv=Mat(Size(640,480),CV_8UC3,Scalar(0,0,0));
    ir=Mat(Size(640,480),CV_8UC3,Scalar(0,0,0));
    //tv=Mat(Size(640,480),CV_8UC3,Scalar(0,0,0));
    //ir=Mat(Size(640,480),CV_8UC3,Scalar(0,0,0));

    continue_key=conf.keys[17];
    esc_key=conf.keys[19];

}

//funkcja wczytująca obrazy i zapisujące je w pamięci
int ImageLoader::load_images(Configuration conf)
{
    initialize_canvas(tv.cols,tv.rows);
    //putText(canvas,"Oczekiwanie na rozpoczecie",Point(70,300),FONT_HERSHEY_SIMPLEX,2,Scalar(255,255,255),2);
    //imshow("canvas",canvas);
    //moveWindow("canvas",0,0);
    //char key=0;
    //while(key!=continue_key)
    //{
    //     key=waitKey();
    //     if(key==esc_key)return 0;
    //}

    if(conf.tcp_ip==0)
    {
        ir=imread(ir_path);
        tv=imread(tv_path);

        if(ir.size()==Size(0,0)||tv.size()==Size(0,0))
            return 2;

        if(tv.rows!=640)
        resize(tv,tv,Size(640,480));

        if(ir.rows!=640)
        resize(ir,ir,Size(640,480));
    }

    if(conf.tcp_ip==1)
    {
        boost::asio::io_service ios;
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket(ios);

        try{socket.connect(endpoint);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 2;}

        char buf_send[]={'g'};
        boost::system::error_code error;
        socket.write_some(boost::asio::buffer(buf_send, 1), error);

        boost::array<uchar,525288> buf_read;
        message_size=boost::asio::read(socket,boost::asio::buffer(buf_read),error);
        //cout<<message_size<<" e1"<<endl;
        socket.close();

        make_images_from_message(buf_read.data());


        if(tv.rows!=640)
        resize(tv,tv,Size(640,480));

        if(ir.rows!=640)
        resize(ir,ir,Size(640,480));

        if(ir.size()==Size(0,0)||tv.size()==Size(0,0))
        return 2;
    }

    return 1;
}

void ImageLoader::make_images_from_message(uchar *tab)
{
    message_size=message_size-1;
    int image_size=sqrt(message_size/2);

    ir=Mat(image_size,image_size,CV_8UC1);
    tv=Mat(image_size,image_size,CV_8UC1);

    int image_bytes_size=message_size/2.0f;

    fov=int(tab[0]);

    for(int i=0;i<image_bytes_size;i++)
        tv.data[i]=tab[i+1];
    for(int i=image_bytes_size;i<2*image_bytes_size;i++)
        ir.data[i-image_bytes_size]=tab[i+1];

    if(image_size!=512)
    {
        resize(tv,tv,Size(512,512));
        resize(ir,ir,Size(512,512));
    }

    cvtColor( tv,tv, CV_GRAY2BGR );
    cvtColor( ir,ir, CV_GRAY2BGR );
}




/*
//funkcja odtwarzająca obrazy na podstawie odebranego ciągu bajtów
void ImageLoader::make_images_from_message(uchar *tab)
{

    message_size=message_size-1;
    //int image_size=sqrt(message_size/2);
    int image_wys=576;//(message_size/704/2);
    int image_szer=704;//(message_size/576/2);

    ir=Mat(image_wys,image_szer,CV_8UC1);
    tv=Mat(image_wys,image_szer,CV_8UC1);

    int image_bytes_size=message_size/2.0f;

    fov=int(tab[0]);

    for(int i=0;i<image_bytes_size;i++)
        tv.data[i]=tab[i+1];
    for(int i=image_bytes_size;i<2*image_bytes_size;i++)
        ir.data[i-image_bytes_size]=tab[i+1];

    if(image_wys!=576 || image_szer!=704)
    {
        resize(tv,tv,Size(704,576));
        resize(ir,ir,Size(704,576));
    }
    cvtColor( tv,tv, CV_GRAY2BGR );
    cvtColor( ir,ir, CV_GRAY2BGR ); 
    imshow("asda",tv);
    waitKey();
    cout<<tv.size()<<" a"<<message_size<<endl;
}
*/
