#ifndef IMPROVEMENT_H
#define IMPROVEMENT_H

#include "gui.h"
#include "operations.h"
#include "zoom.h"
#include "imageimprovement.h"
#include "configuration.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS ZWIĄZANY Z POPRAWĄ WYNIKÓW KALIBRACJI
//=====================================================================================

class Improvement : public Operations, public Zoom
{
public:

    //konstruktor klasy
    Improvement(Gui gui, ImageLoader images, Configuration conf);

    //wektory przechowujące zapisane punkty na obu obrazach
    vector< Point > tv_points;vector< Point > ir_points;

    //aktualnie wyświetlane obrazy
    Mat tv;Mat ir;

    //originalne obrazy przechowywane w pamiędzu
    Mat tv_original;Mat ir_original;

    //funkcja umożlwiwiająca wybór aktywnego punktu oraz zmianę jego położenia
    void improve();

    //znaki definiujące używane klawisze klawisze z pliku konfiguracyjnego
    char next_cross;
    char change_end;
    char pick_image_key;
    char delete_key;

    //numery aktywnych puntów na każdym z obrazów
    int ir_akt=0;int tv_akt=0;

    //zmienna określająca aktywny obraz
    int pick_image_value=1;

    //wskaźniki na obiekty umożliwiające poprawę punktów na danym obrazie
    ImageImprovement *tv_improvement;
    ImageImprovement *ir_improvement;
};

#endif // IMPROVEMENT_H
