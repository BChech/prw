#include "storage.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA ZAPIS MACIERZY PRZEKSZTALCENIA
//=====================================================================================

//pierwszy konstruktor klasy
Storage::Storage(Mat transformation,string transformation_matrix_path)
{
    FileStorage file(transformation_matrix_path,cv::FileStorage::WRITE);
    file<<"transformation_matrix"<<transformation;
    file.release();
}

//drugi konstruktor klasy
Storage::Storage(Configuration conf,ImageLoader image_loader)
{
    tcp_host_name=conf.tcp_host_name;
    tcp_port_number=conf.tcp_port_number;
    fov=image_loader.fov;
}

//funkcja wysyłająca wyznaczoną macierz transformacji
int Storage::send_transformation(Mat transformation,Configuration conf)
{
    if(conf.tcp_ip==1)
    {
        boost::asio::io_service ios;
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket(ios);

        try{socket.connect(endpoint);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        char buf_send[26];
        make_message_from_transformation(transformation,buf_send);
        boost::system::error_code error;
        socket.write_some(boost::asio::buffer(buf_send, 26), error);

        //zapis na dysk
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        file<<"transformation_matrix"<<transformation;
        file.release();
    }

    if(conf.tcp_ip==0)
    {
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        file<<"transformation_matrix"<<transformation;
        file.release();
    }

    return 1;
}

//funkcja tworząca łańcuch bajtów na podstawie macierzy transformacji
void Storage::make_message_from_transformation(Mat transformation,char *tab)
{
    tab[0]='t';

    tab[1]=fov;

    for(int i=0;i<24;i++)
        tab[i+2]=transformation.data[i];
}

//funkcja wysyłająca wyznaczoną macierz kątów widzenia
int Storage::send_matrix(Configuration conf,cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2)
{
    if(conf.tcp_ip==1)
    {
        boost::asio::io_service ios;
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket(ios);

        try{socket.connect(endpoint);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        int bufSize = sizeof(float)*4+2;
        char buf_send[bufSize];
        make_message_from_matrixC(camera_matrix1,camera_matrix2, buf_send);
        boost::system::error_code error;
        socket.write_some(boost::asio::buffer(buf_send, bufSize), error);


        boost::asio::io_service ios2;
        boost::asio::ip::tcp::endpoint endpoint2(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket2(ios2);

        try{socket2.connect(endpoint2);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        char buf_send2[bufSize];
        make_message_from_matrixD(camera_matrix1,camera_matrix2, buf_send2);
        boost::system::error_code error2;
        socket2.write_some(boost::asio::buffer(buf_send2, bufSize), error2);

        //zapis do pliku
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        cv::Mat_<float> laczona;
        laczona.push_back(camera_matrix1);
        laczona.push_back(camera_matrix2);
        file<<"transformation_matrix"<<laczona;
        file.release();
    }

    if(conf.tcp_ip==0)
    {
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        cv::Mat_<float> laczona;
        laczona.push_back(camera_matrix1);
        laczona.push_back(camera_matrix2);
        file<<"transformation_matrix"<<laczona;
        file.release();
    }

    return 1;
}
void Storage::make_message_from_matrixC(cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2, char *tab)
{
    tab[0]='c';

    tab[1]=fov;

    cout<<camera_matrix1<<endl<<camera_matrix2<<endl;

    for(uint i = 0; i < sizeof(float); i++)
    {
        tab[i+ 2] = camera_matrix1.data[i];
        tab[i+ 2+sizeof(float)] = camera_matrix1.data[sizeof(float)*2+i];
        tab[i +2+sizeof(float)*2] = camera_matrix1.data[sizeof(float)*4+i];
        tab[i +2+sizeof(float)*3] = camera_matrix1.data[sizeof(float)*5+i];
    }
}

void Storage::make_message_from_matrixD(cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2, char *tab)
{
    tab[0]='d';

    tab[1]=fov;

    cout<<camera_matrix1<<endl<<camera_matrix2<<endl;

    for(uint i = 0; i < sizeof(float); i++)
    {
        tab[i+ 2] = camera_matrix2.data[i];
        tab[i+ 2+sizeof(float)] = camera_matrix2.data[sizeof(float)*2+i];
        tab[i +2+sizeof(float)*2] = camera_matrix2.data[sizeof(float)*4+i];
        tab[i +2+sizeof(float)*3] = camera_matrix2.data[sizeof(float)*5+i];
    }
}

int Storage::send_transformationv2(Configuration conf,Mat transformation)
{
    if(conf.tcp_ip==1)
    {
        boost::asio::io_service ios;
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket(ios);

        try{socket.connect(endpoint);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        int bufSize = sizeof(float)*4+2;
        char buf_send[bufSize];
        make_message_from_transformationv22(transformation, buf_send);
        boost::system::error_code error;
        socket.write_some(boost::asio::buffer(buf_send, bufSize), error);

        boost::asio::io_service ios2;
        boost::asio::ip::tcp::endpoint endpoint2(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket2(ios2);

        try{socket2.connect(endpoint2);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        char buf_send2[bufSize];
        make_message_from_transformationv21(transformation, buf_send2);
        boost::system::error_code error2;
        socket2.write_some(boost::asio::buffer(buf_send2, bufSize), error2);

        for(uint i = 0; i < sizeof(float)*4+2; i++)
            cout << (int)buf_send[i] << "|";
        cout << endl;
        for(uint i = 0; i < sizeof(float)*4+2; i++)
            cout << (int)buf_send2[i] << "|";
        cout << endl;

        //zapis do pliku
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        //cv::Mat_<float> laczona;
        //laczona.push_back(camera_matrix1);
        //laczona.push_back(camera_matrix2);
        //file<<"transformation_matrix"<<laczona;
        file.release();
    }

    if(conf.tcp_ip==0)
    {
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        int bufSize = sizeof(float)*4+2;
        char buf_send[bufSize];
        make_message_from_transformationv21(transformation, buf_send);
        char buf_send2[bufSize];
        make_message_from_transformationv22(transformation, buf_send2);
        for(uint i = 0; i < sizeof(float)*4+2; i++)
            cout << (int)buf_send[i] << "|";
        cout << endl;
        for(uint i = 0; i < sizeof(float)*4+2; i++)
            cout << (int)buf_send2[i] << "|";
        cout << endl;
        file.release();
    }

    return 1;
}

void Storage::make_message_from_transformationv21(Mat transformation, char *tab)
{
    tab[0]='d';

    tab[1]=fov;

    Mat_<float> size = Mat_<float>(2,1);
    size[0][0] = (float)(512/2);
    size[1][0] = (float)(1);

    for(uint i = 0; i < sizeof(float); i++)
    {
        tab[i+ 2] = size.data[sizeof(float)+i];
        tab[i+ 2+sizeof(float)] = size.data[i];
        tab[i +2+sizeof(float)*2] = size.data[sizeof(float)+i];
        tab[i +2+sizeof(float)*3] = size.data[i];
    }
}

void Storage::make_message_from_transformationv22(Mat transformation, char *tab)
{
    tab[0]='c';

    tab[1]=fov;

    Mat_<float> size = Mat_<float>(1,1);
    size[0][0] = (float)(512/2);

    for(uint i = 0; i < sizeof(float); i++)
    {
        tab[i+ 2] = transformation.data[i];
        tab[i+ 2+sizeof(float)] = size.data[i];
        tab[i +2+sizeof(float)*2] = transformation.data[sizeof(float)*4+i];
        tab[i +2+sizeof(float)*3] = size.data[i];
    }
}


int Storage::send_epipolar(Configuration conf, cv::Mat result)
{
    if(conf.tcp_ip==1)
    {
        boost::asio::io_service ios;
        boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(tcp_host_name), tcp_port_number);
        boost::asio::ip::tcp::socket socket(ios);

        try{socket.connect(endpoint);}
        catch(std::exception& e)
        {std::cerr<<e.what()<<std::endl;return 0;}

        int bufSize = sizeof(float)*2+2;
        char buf_send[bufSize];
        make_message_from_epipolar(result,buf_send);
        boost::system::error_code error;
        socket.write_some(boost::asio::buffer(buf_send, 26), error);
    }

    if(conf.tcp_ip==0)
    {
        FileStorage file(conf.transformation_matrix_path,cv::FileStorage::WRITE);
        file<<"transformation_matrix"<<result;
        file.release();
    }

    return 1;
}

void Storage::make_message_from_epipolar(Mat transformation,char *tab)
{
    tab[0]='e';
    //transformation.at<float>(0,0)=0.6;
    //transformation.at<float>(0,1)=16;
    tab[1]=fov;

    for(uint i=0;i<sizeof(float)*2;i++)
        tab[i+2]=transformation.data[i];
}
