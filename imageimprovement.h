#ifndef IMAGEIMPROVEMENT_H
#define IMAGEIMPROVEMENT_H

#include "imageloader.h"
#include "operations.h"
#include "zoom.h"

//=======================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS POPRAWY WYNIKÓW KALIBRACJI ZWIĄZANY Z JEDNYM OBRAZEM
//=======================================================================================

class ImageImprovement : public Operations,public Zoom
{
public:

    //konstruktor klasy
    ImageImprovement(Mat image,vector< Point > saved_points, char *keys, Scalar active_color, Scalar written_color, float *parameters);

    //funkcja uaktualniająca wektor zapisanych punuktów na podstawie aktualnie wciśniętego klawisza kalwiatury
    void update_crosses(vector<Point> &saved_points, int akt, char key);

    Mat image_original;            //obraz oryginalny TV/IR
    Mat image_original_with_cross; //obraz z wrysowanymi wskazanymi krzyżykami
    Mat image_processed;           //obraz przybliżony wyznaczony na podstawie obrazeu z wrysowanymi krzyżykami
    vector< Point2f > points;      //wektor aktualnie wskazanych punktów

};

#endif // IMAGEIMPROVEMENT_H
