#include "operations.h"

//=====================================================================================
// KLASA ZAWIERAJĄCA PODSTAWOWE OPERACJE UDOSTĘPNIANE WSZYSTKIM INNYM KLASOM
//=====================================================================================

//domyślny konstruktor klasy
Operations::Operations(){}

//funkcja tworząca łańcuch znaków na podstawie wartości zmiennoprzecinkowej
string Operations::make_string_from_value(float value)
{
    std::vector<std::string> second_text;
    std::stringstream buff;
    buff<<value;
    string s=buff.str();

    return s;
}

//funkcja rysująca krzyżyk w przestrzeni obrazu
void Operations::draw_cross(Mat image, QPoint punkt, Scalar kolor, bool active)
{
    Point poziom_l,poziom_p,pion_g,pion_d;
    poziom_l.x=punkt.x();
    poziom_l.y=punkt.y()-length;
    poziom_p.x=punkt.x();
    poziom_p.y=punkt.y()+length;

    pion_g.x=punkt.x()-length;
    pion_g.y=punkt.y();
    pion_d.x=punkt.x()+length;
    pion_d.y=punkt.y();
    if(active)
    {
        line(image,poziom_l,poziom_p,kolor,activeThickness);
        line(image,pion_g,pion_d,kolor,activeThickness);
    }
    else
    {
        line(image,poziom_l,poziom_p,kolor,inactiveThickness);
        line(image,pion_g,pion_d,kolor,inactiveThickness);
    }
    //line(image,Point(punkt.x,punkt.y)-Point(0,length),Point(punkt.x,punkt.y)+Point(0,length),kolor,thickness);
    //line(image,Point(punkt.x,punkt.y)-Point(length,0),Point(punkt.x,punkt.y)+Point(length,0),kolor,thickness);
}

void Operations::draw_cross(Mat image, Point punkt, Scalar kolor)
{
    line(image,punkt-Point(0,length),punkt+Point(0,length),kolor,inactiveThickness);
    line(image,punkt-Point(length,0),punkt+Point(length,0),kolor,inactiveThickness);
}
//funkcja inicjalizująca wspólną przestrzeń obrazową
void Operations::initialize_canvas(int width,int height)
{
    canvas=Mat(height/*+text_height*/,width*2,CV_8UC3,Scalar(0,0,0));
    fusion_canvas=Mat(height,width,CV_8UC3,Scalar(0,0,0));
}

//funkcja wyświetlająca tekst w danym miejscu we wspólnej przestrzeni obrazowej
void Operations::draw_text(Point pos, int error, std::string text)
{
    if(error==0)
    {
        rectangle(canvas,Rect(pos.x,pos.y-20,500,30),Scalar(0,0,0),-1);
        putText(canvas,text,pos,FONT_HERSHEY_SIMPLEX,0.8,Scalar(255,255,255),1);
    }

    if(error==1)
    {
        rectangle(canvas,Rect(pos.x,pos.y-30,700,40),Scalar(0,0,0),-1);
        putText(canvas,"ERROR: ",pos,FONT_HERSHEY_SIMPLEX,1,Scalar(0,0,255),2);
        pos.x=pos.x+120;
        putText(canvas,text,pos,FONT_HERSHEY_SIMPLEX,1,Scalar(255,255,255),1);
    }
}

//funkcja uaktualniająca wspólną przestrzeń obrazowąna podstawie obrazów TV oraz IR
void Operations::update_canvas(Mat one, Mat two, float rPar, QPoint pp)
{
    int height=one.rows;
    int width=one.cols;

    Mat one2;
    resize(one, one2, Size(width*rPar, height*rPar));

    Mat two2;
    resize(two, two2, Size(width*rPar, height*rPar));

    //int a=width-width*rPar, b=height-height*rPar;

    for(int i=0;i<height;i++)
        for(int j=0;j<width;j++)
    canvas.at<Vec3b>(i,j)=one2.at<Vec3b>(i+pp.y(),j+pp.x());

    for(int i=0;i<height;i++)
        for(int j=width;j<2*width;j++)
    canvas.at<Vec3b>(i,j)=two2.at<Vec3b>(i+pp.y(),j+pp.x()-width);

    /*if(picked_image!=0)
    {
        if(picked_image==1)
        {
            circle(one,Point(20,20),15,Scalar(0,255,0),-1);
            circle(two,Point(20,20),15,Scalar(0,0,255),-1);
        }

        if(picked_image==-1)
        {
            circle(one,Point(20,20),15,Scalar(0,0,255),-1);
            circle(two,Point(20,20),15,Scalar(0,255,0),-1);
        }
    }*/

    //imshow("canvas",canvas);
    //moveWindow("canvas",0,0);

    ////cvSetWindowProperty("canvas", CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);
}

//funkcja wykonująca fuzję na podstawie obrazów użytych do kalibracji
void Operations::simple_fusion_canvas(Mat one,Mat two)
{
    int height=one.rows;
    int width=one.cols;

    for(int i=0;i<height;i++)
        for(int j=0;j<width;j++)
            fusion_canvas.at<Vec3b>(i,j)=Vec3b(0,0,0);

    for(int i=0;i<height;i++)
        for(int j=0;j<width;j++)
    {
         //fusion_canvas.at<Vec3b>(i,j+width/2.0f).val[0]=one.at<Vec3b>(i,j).val[0];
         //fusion_canvas.at<Vec3b>(i,j+width/2.0f).val[2]=two.at<Vec3b>(i,j).val[0];
            fusion_canvas.at<Vec3b>(i,j).val[0]=one.at<Vec3b>(i,j).val[0];
            fusion_canvas.at<Vec3b>(i,j).val[2]=two.at<Vec3b>(i,j+(640.0-512.0)/5.0).val[0];
    }

    //imshow("canvas",canvas);
    //moveWindow("canvas",0,0);
}
