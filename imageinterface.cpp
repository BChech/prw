#include "imageinterface.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS ZWIĄZANY Z JEDNYM OBRAZEM
//=====================================================================================

//konstruktor klasy
/*ImageInterface::ImageInterface(Mat image, char *keys, Scalar active_color, Scalar written_color, float *parameters)//ImageLoader images)
{
    up=keys[0];left=keys[1];down=keys[2];right=keys[3];
    zoom_in=keys[4];zoom_out=keys[5];save=keys[6];
    cross_color=active_color;crosses_written_color=written_color;
    rect_resize=parameters[0];speed_initial=parameters[1];speed_reduction=parameters[2];min_close_points_distance=parameters[7];
    speed=speed_initial;
    width=image.cols;height=image.rows;
    rect_width=width;rect_height=height;
    rect_middle=Point(width/2,height/2);
    cross_pos=Point(width/2.0f,height/2.0f);
    image.copyTo(image_original);
    image.copyTo(image_original_with_cross);
    length=parameters[3];
    thickness=parameters[4];
    draw_cross(image_original_with_cross,cross_pos,cross_color);
    image_original_with_cross.copyTo(image_processed);
    rect=Rect(rect_corner.x,rect_corner.y,rect_width,rect_height);
}*/
ImageInterface::ImageInterface(Mat original)
{
    original.copyTo(image_original);
    original.copyTo(image_original_with_cross);
    original.copyTo(image_processed);
}

void ImageInterface::addPoint()
{
    points.push_back(KalibPoint(QPoint(360,240),false));
    update_image();
}

void ImageInterface::update_image()
{
    image_original.copyTo(image_original_with_cross);
    for(size_t i=0;i<points.size();i++)
    {
        if(points[i].active==true)
            draw_cross(image_original_with_cross,points[i].point,Scalar(0,0,255),true);
        else
            draw_cross(image_original_with_cross,points[i].point,Scalar(0,255,255),false);
    }
}

int ImageInterface::checkPointPressed(QPoint pressed, int &crossX, int &crossY)
{
    for(size_t i=0;i<points.size();i++)
        if(points[i].pointRect.contains(pressed))
        {
            crossX = pressed.x()-points[i].point.x();
            crossY = pressed.y()-points[i].point.y();
            return i;
        }
    crossX=0;
    crossY=0;
    return -1;
}

void ImageInterface::setActive(int i)
{
    points[i].active=true;
}

void ImageInterface::unActive()
{
    for(size_t i=0;i<points.size();i++)
        points[i].active=false;
}

void ImageInterface::setPoint(int i, QPoint p)
{
    points[i].point=p;
    points[i].pointRect.moveCenter(points[i].point);
}

void ImageInterface::deletePoint(int i)
{
    points.erase(points.begin()+i);
}

//funkcja licząca minimalną średnią odlęgłość między punktami
float ImageInterface::minAverangeDistance()
{
    float suma=0.0;
    for(size_t i=0;i<points.size()-1;i++)
        for(size_t j=i+1;j<points.size();j++)
            suma+=distance(points[i],points[j]);
    suma/=points.size()*(points.size()-1)/2;
    return suma;
}

//funkcja obliczająca dystans pomiędzy dwoma punktami
float ImageInterface::distance(KalibPoint a,KalibPoint b)
{
    return sqrt((a.point.x()-b.point.x())*(a.point.x()-b.point.x())+(a.point.y()-b.point.y())*(a.point.y()-b.point.y()));
}
/*
//funkcja wyznaczająca współczynnik kierunkowy prostej łączącej dwa punkty
float ImageInterface::gradient(Point a,Point b)
{
    float grad=(b.y-a.y)/float(b.x-a.x);
    if(grad>100000)grad=100000;
    if(grad<-100000)grad=-100000;

    return grad;
}

//funkcja wyznaczająca odwrotnośc współczynnika prostej łączącej dwa punkty
float ImageInterface::gradient_inv(Point a,Point b)
{
    float grad=(b.x-a.x)/float(b.y-a.y);
    if(grad>100000)grad=100000;
    if(grad<-100000)grad=-100000;

    return grad;
}

//funkcja wyznaczająca średnią odległość pomiędzy wszystkimi wskazanymi punktami
float ImageInterface::average_distance()
{
    float average_dist=0;
    int size=points.size();

    if(size==0)return 0;

    for (int i=0;i<size;i++)
        for(int j=0;j<size;j++)
            if(i!=j)
            average_dist=average_dist+distance(points[i],points[j]);

    int vertex_number=size*(size-3)/float(2);
    if(vertex_number==0)vertex_number=3;

    average_dist=average_dist/float(vertex_number);

    return average_dist;
}

//funkcja wyznaczająca maksymalny współczynnik kierunkowy prostej łączącej dwa punkty spośród wszystkich par wskazanych punktów
float ImageInterface::max_gradient_diff()
{
    float max_gradient_diff=0;
    int size=points.size();
    float gradient_descent[size-1];

    for(int i=0;i<size-1;i++)
    {
        gradient_descent[i]=gradient(points[i],points[i+1]);
        if(gradient_descent[i]<0)gradient_descent[i]=-gradient_descent[i];
    }

    for(int i=0;i<size-2;i++)
    {
        float gradient_diff=gradient_descent[i+1]-gradient_descent[i];
        if(gradient_diff<0)gradient_diff=-gradient_diff;
        if(gradient_diff>max_gradient_diff)max_gradient_diff=gradient_diff;
    }

    if(max_gradient_diff<0)max_gradient_diff=-max_gradient_diff;
    return max_gradient_diff;
}

//funkcja wyznaczająca maksymalny odwrotny współczynnik kierunkowy prostej łączącej dwa punkty spośród wszystkich par wskazanych punktów
float ImageInterface::max_gradient_diff_inv()
{
    float max_gradient_diff=0;
    int size=points.size();
    float gradient_descent[size-1];

    for(int i=0;i<size-1;i++)
    {
        gradient_descent[i]=gradient_inv(points[i],points[i+1]);
        if(gradient_descent[i]<0)gradient_descent[i]=-gradient_descent[i];
    }

    for(int i=0;i<size-2;i++)
    {
        float gradient_diff=gradient_descent[i+1]-gradient_descent[i];
        if(gradient_diff<0)gradient_diff=-gradient_diff;
        if(gradient_diff>max_gradient_diff)max_gradient_diff=gradient_diff;
    }

    if(max_gradient_diff<0)max_gradient_diff=-max_gradient_diff;
    return max_gradient_diff;
}

//funkcja uaktualniająca położenie krzyżyka na podstawie wciśniętego kalwisza klawaitury
void ImageInterface::update_cross(char key)
{
    image_original.copyTo(image_original_with_cross);
    update_point((cross_pos),key);

    if(key==save)
    {
        Point2f point(cross_pos.x,cross_pos.y);

        float min_distance=100;
        int size=points.size();

        for(int i=0;i<size;i++)
        {
            float akt_distance=distance(points[i],point);
            if(akt_distance<min_distance)
                min_distance=akt_distance;
        }

        if(min_distance>min_close_points_distance)
        {
            points.push_back(point);
            point_saved=1;
        }
    }

    for(int i=0;i<points.size();i++)
    {
        draw_cross(image_original_with_cross,points[i],crosses_written_color);
    }

    draw_cross(image_original_with_cross,cross_pos,cross_color);

    rect=Rect(rect_corner.x,rect_corner.y,rect_width,rect_height);
    image_processed=image_original_with_cross(rect);
    resize(image_processed,image_processed,Size(width,height),0,0,CV_INTER_NN);
}

*/
