#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "imageloader.h"
#include "operations.h"
#include "configuration.h"
#include "imageinterface.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA WYKONANIE KALIBRACJI I WYŚWIETLENIE WYNIKÓW
//=====================================================================================

class Calibration : public Operations
{
public:

    //konstruktor klasy
    Calibration(ImageLoader images, Configuration conf);

    vector< QPoint > tv_points;           //
    vector< QPoint > ir_points;           //wektory zapisanych punktów
    vector< Point2f > ir_at_tv_points;    //wektor przekształconych punktów z obrazu IR

    //funkcja wykonanująca kalibrację i wyświetlająca jej wyniki
    int calib(ImageInterface intTV, ImageInterface intIR);

    Mat transformationMatrix_ir2tv;       //macierz przekształcenia
    Mat tv;Mat ir;                        //obrazy z zaznaczonymi krzyzykami
    Mat tv_original;Mat ir_original;      //oryginalne obrazy
    Mat ir_transformed;                   //przekształcony obraz IR

    int point_saved=0;                    //liczba zapisanych punktów
    float blad, angle;
    int tx, ty;
    //parametry definiujące kolory krzyżyków odczytane z pliku konfiguracyjnego
    Scalar crosses_projection_color;
    Scalar crosses_written_color;
};

#endif // CALIBRATION_H
