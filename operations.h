#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/video.hpp"
#include "QPoint"

using namespace std;
using namespace cv;

//=====================================================================================
// KLASA ZAWIERAJĄCA PODSTAWOWE OPERACJE UDOSTĘPNIANE WSZYSTKIM INNYM KLASOM
//=====================================================================================

class Operations
{
public:

    //konstruktor klasy
    Operations();

    //funkcja rysująca krzyżyk w przestrzeni obrazu
    void draw_cross(Mat image, QPoint punkt, Scalar kolor, bool active);

    void draw_cross(Mat image,Point punkt,Scalar kolor);

    int activeThickness=2;
    int inactiveThickness=1;
    int length=15;    //parametry krzyżyków odczytane z pliku konfiguracyjnego
    Mat canvas;                       //wspolna przestrzeń na którym wyświetlane są obrazy TV praz IR
    Mat fusion_canvas;
    int text_height=140;              //wysokość pola tekstowago we wspólnej przestrzeni

    //segmentation fault
    //funkcja uaktualniająca wspólną przestrzeń obrazowąna podstawie obrazów TV oraz IR
    void update_canvas(Mat one, Mat two/*, int picked_image*/, float rPar=1.0, QPoint pp=QPoint(0,0));

    //sf
    //funkcja wykonująca fuzję na podstawie obrazów użytych do kalibracji
    void simple_fusion_canvas(Mat one,Mat two);

    //funkcja inicjalizująca wspólną przestrzeń obrazową
    void initialize_canvas(int width, int height);

    //funkcja wyświetlająca tekst w danym miejscu we wspólnej przestrzeni obrazowej
    void draw_text(Point pos,int error, string text);

    //funkcja tworząca łańcuch znaków na podstawie wartości zmiennoprzecinkowej
    string make_string_from_value(float value);
};

#endif // OPERATIONS_H
