#ifndef IMAGEINTERFACE_H
#define IMAGEINTERFACE_H

#include "imageloader.h"
#include "operations.h"

#include <QRect>

struct KalibPoint
{
    KalibPoint(QPoint p, bool _active=true)
    {
        point=p;
        active=_active;
        pointRect = QRect(QPoint(),QSize(30,30));
        pointRect.moveCenter(point);
    }

    QPoint point;
    bool active;
    QRect pointRect;
};
//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA INTERFEJS ZWIĄZANY Z JEDNYM OBRAZEM
//=====================================================================================

class ImageInterface : public Operations/*, public Zoom*/
{
public:

    //konstruktor klasy
    ImageInterface(Mat original/*, char *keys, Scalar active_color, Scalar written_color, float *parameters*/);

    //dodawanie punktu
    void addPoint();

    //uaktualnienie obrazu;
    void update_image();

    //sprawdzenie czy kliknięto w punkt
    int checkPointPressed(QPoint pressed, int &crossX, int &crossY);

    //ustawienie aktywnego punktu
    void setActive(int i);

    //ustawienie wszystkich punktów na nieaktywne
    void unActive();

    //uaktualnienie pozycji punktu
    void setPoint(int i,QPoint p);

    //usunięcie punktu
    void deletePoint(int i);

    //funkcja uaktualniająca położenie krzyżyka na podstawie wciśniętego kalwisza klawaitury
    //void update_cross(char key);

    //funkcja licząca minimalną średnią odlęgłość między punktami
    float minAverangeDistance();
    //funkcja obliczająca dystans pomiędzy dwoma punktami
    float distance(KalibPoint a,KalibPoint b);

    //funkcja wyznaczająca współczynnik kierunkowy prostej łączącej dwa punkty
    //float gradient(Point a,Point b);
    //funkcja wyznaczająca odwrotnośc współczynnika prostej łączącej dwa punkty
    //float gradient_inv(Point a,Point b);
    //funkcja wyznaczająca maksymalny współczynnik kierunkowy prostej łączącej dwa punkty spośród wszystkich par wskazanych punktów
    //float max_gradient_diff();
    //funkcja wyznaczająca maksymalny odwrotny współczynnik kierunkowy prostej łączącej dwa punkty spośród wszystkich par wskazanych punktów
    //float max_gradient_diff_inv();
    //funkcja wyznaczająca średnią odległość pomiędzy wszystkimi wskazanymi punktami
    //float average_distance();

    Mat image_original;             //obraz oryginalny TV/IR
    Mat image_original_with_cross;  //obraz z wrysowanymi wskazanymi krzyżykami
    Mat image_processed;            //obraz przybliżony wyznaczony na podstawie obrazeu z wrysowanymi krzyżykami
    vector< KalibPoint > points;         //wektor zawierający zapisane wskazane punkty

    float min_close_points_distance;//parametr odczytany z pliku konfiguracyjnego
};

#endif // IMAGEINTERFACE_H
