#ifndef STORAGE_H
#define STORAGE_H

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "imageloader.h"
#include "configuration.h"

using namespace cv;
using namespace std;

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA ZAPIS MACIERZY PRZEKSZTALCENIA
//=====================================================================================

class Storage
{
public:

    //konstruktory klasy
    Storage(Mat transformation, string transformation_matrix_path);
    Storage(Configuration conf, ImageLoader image_loader);

    //funkcja wysy`łająca wyznaczoną macierz transformacji
    int send_transformation(Mat transformation, Configuration conf);
    //funkcja tworząca łańcuch bajtów na podstawie macierzy transformacji
    void make_message_from_transformation(Mat transformation,char *tab);

    int send_transformationv2(Configuration conf, Mat transformation);
    //funkcja tworząca łańcuch bajtów na podstawie macierzy transformacji
    void make_message_from_transformationv21(Mat transformation,char *tab);
    void make_message_from_transformationv22(Mat transformation,char *tab);

    int send_matrix(Configuration conf,cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2);
    //void make_message_from_matrix(cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2, char *tab);
    void make_message_from_matrixC(cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2, char *tab);
    void make_message_from_matrixD(cv::Mat_<float> camera_matrix1, cv::Mat_<float> camera_matrix2, char *tab);

    //
    int send_epipolar(Configuration conf, cv::Mat result);
    void make_message_from_epipolar(Mat transformation,char *tab);




    string tcp_host_name;   //adres ip układu jetson
    int tcp_port_number;    //numer portu aplikacji uruchomionej na platformie jetson
    int fov;                //przekazywany kąt widzenia kamer
};

#endif // STORAGE_H
