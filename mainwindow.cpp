#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog> //okienko wyboru pliku konfiguracyjnego
#include <QWhatsThis>
#include <QMessageBox>

#define IMAGE_WIDTH 640
#define IMAGE_HEIGHT 480

//konstruktor nowego okna
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    odczytanieParametrow();
    ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
    ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
    resizePar=1.0;
    pp=QPoint(0,0);

    //chowanie kalibracji 2-punktowej
    ui->groupBox_5->hide();
}

void MainWindow::setShortcuts()
{
    ui->pushButton_s->setShortcut((Qt::Key)(configuration->keys[0]+('A'-'a')));
    ui->pushButton_a->setShortcut((Qt::Key)(configuration->keys[1]+('A'-'a')));
    ui->pushButton_w->setShortcut((Qt::Key)(configuration->keys[2]+('A'-'a')));
    ui->pushButton_d->setShortcut((Qt::Key)(configuration->keys[3]+('A'-'a')));
    ui->pushButton_SizeUp->setShortcut((Qt::Key)(configuration->keys[4]+('A'-'a')));
    ui->pushButton_SizeDown->setShortcut((Qt::Key)(configuration->keys[5]+('A'-'a')));

    ui->pushButton_WyslanieTransformacji->setShortcut((Qt::Key)(configuration->keys[8]+('A'-'a')));

    ui->pushButton_UsunAktywnePunkty->setShortcut((Qt::Key)(configuration->keys[11]+('A'-'a')));
    ui->pushButton_min_number_of_points_pairs_down->setShortcut((Qt::Key)(configuration->keys[12]+('A'-'a')));
    ui->pushButton_average_distance_between_points_down->setShortcut((Qt::Key)(configuration->keys[13]+('A'-'a')));

    ui->pushButton_PokazFuzje->setShortcut((Qt::Key)(configuration->keys[15]+('A'-'a')));
    ui->pushButton_Reset->setShortcut((Qt::Key)(configuration->keys[16]+('A'-'a')));
    ui->pushButton_PobranieObrazow->setShortcut((Qt::Key)(configuration->keys[17]+('A'-'a')));

}

//destruktor okna
MainWindow::~MainWindow()
{
    delete ui;
    if(configuration!=NULL) delete configuration;
    if(imageloader!=NULL)   delete imageloader;
    if(interfaceTV!=NULL)   delete interfaceTV;
    if(interfaceIR!=NULL)   delete interfaceIR;
    if(calibration!=NULL)   delete calibration;
}

//obsługa wyboru pliku konfiguracyjnego
void MainWindow::on_pushButton_WybierzPlik_clicked()
{
    QString config_filename=QFileDialog::getOpenFileName(this, tr("Wybierz plik konfiguracyjny"), "/home/", "Pliki yaml (*.yaml)");
    if(config_filename==NULL) return;
    ui->lineEdit_SciezkaPliku->setText(config_filename);
    odczytanieParametrow();
}

//wczytywanie pliku konfiguracyjnego po wciśnieciu Enter
void MainWindow::on_lineEdit_SciezkaPliku_returnPressed()
{
    odczytanieParametrow();
}

//wczytywanie konfiguracji z pliku do programu
bool MainWindow::odczytanieParametrow()
{
    QString config_filename=ui->lineEdit_SciezkaPliku->text();
    if(configuration!=NULL) delete configuration;
    configuration = new Configuration(config_filename.toStdString());//stworzenie obiektu który odczyta plik konfiguracyjny

    if(configuration->config_file_found==0)
    {
        ui->label_komunikaty_l->setText("Nie udało się wczytać pliku konfiguracyjnego");
        ui->label_komunikaty_p->setText("Wybierz inny plik konfiguracyjny");
        ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
        ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
        return true;
    }
    else
    {
        ui->label_min_lparpkt->setText("Min liczba par punktów: "+QString::number(configuration->parameters[5]));
        ui->label_minSredniaOdl->setText("Min średnia odległość: "+QString::number(configuration->parameters[6]));
        ui->label_komunikaty_l->setText("Zakończono wczytywanie pliku konfiguracyjnego");
        ui->lineEdit_k1_x->setText(QString::number(configuration->angles[0]));
        ui->lineEdit_k1_y->setText(QString::number(configuration->angles[1]));
        ui->lineEdit_k2_x->setText(QString::number(configuration->angles[2]));
        ui->lineEdit_k2_y->setText(QString::number(configuration->angles[3]));
        ui->label_komunikaty_p->setText("Pobierz obrazy");
        ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
        ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
        //ustawianie skrótów
        setShortcuts();
        return false;
    }
}

//pobiera obrazów z platformy Jetson lub z dysku i wyświetlenie ich na obszarze roboczym
void MainWindow::on_pushButton_PobranieObrazow_clicked()
{
    //odczytanie obrazów z platformy jetson lub z dysku
    if(configuration && configuration->config_file_found==1)
    {
        if(imageloader!=NULL) delete imageloader;
        imageloader = new ImageLoader(*configuration);
        int images_status=imageloader->load_images(*configuration);//wczytywanie obrazów
        if(images_status==1)
        {
             ui->label_komunikaty_l->setText("Zakończono wczytywanie obrazu");
             ui->label_komunikaty_p->setText("Dodaj punkty");

             if(interfaceTV!=NULL) delete interfaceTV;
             interfaceTV = new ImageInterface(imageloader->tv);

             if(interfaceIR!=NULL) delete interfaceIR;
             interfaceIR = new ImageInterface(imageloader->ir);

             imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
             ui->widget_kamery->showImage(imageloader->canvas);

             if(calibration!=NULL) delete calibration;
             calibration = new Calibration(*imageloader, *configuration);
        }
        else if(images_status==2)
        {
            ui->label_komunikaty_l->setText("Wczytywanie obrazow nie powiodlo sie. Problemy z polaczeniem");
            ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
            ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
        }
        else  ui->label_komunikaty_l->setText("Zamykanie aplikacji.");
    }
    else
    {
        ui->label_komunikaty_l->setText("Brak pliku konfiguracyjnego");
        ui->label_komunikaty_p->setText("Wybierz plik konfiguracyjny");
        ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
        ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
    }
}

//dodawanie nowych punktów do obszaru roboczego
void MainWindow::on_pushButton_DodajPunkt_clicked()
{
    if(imageloader && calibration && interfaceTV && interfaceIR && czyWidgetKameryPokazujeFuzje==false)
    {
        interfaceTV->addPoint();
        interfaceIR->addPoint();
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
        calibration->point_saved++;
        ui->label_komunikaty_p->setText("Wciśnij punkt, żeby przesunąć lub dodaj nowy punkt");
        ui->label_Lparpunktow->setText("Liczba par punktów: "+QString::number(calibration->point_saved));
    }
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!interfaceTV)
        ui->label_komunikaty_l->setText("Brak interfejsu TV");
    else if(!interfaceIR)
        ui->label_komunikaty_l->setText("Brak interfejsu IR");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//obsługa ustawiania punktów na ekranie roboczym za pomocą myszki
void MainWindow::on_widget_kamery_signal_mouseLeftPress(QMouseEvent *event)
{
    if(imageloader && interfaceTV && interfaceIR && calibration->point_saved>0)
    {
        if(czyWidgetKameryPokazujeFuzje==false)
        {
            QPoint positionTV = ui->widget_kamery->mapFromGlobal(event->globalPos());
            QPoint positionIR = ui->widget_kamery->mapFromGlobal(event->globalPos())-QPoint(IMAGE_WIDTH,0);

            positionTV+=pp;
            positionTV/=resizePar;
            positionIR+=pp;
            positionIR/=resizePar;

            int indeks;
            if((indeks=interfaceTV->checkPointPressed(positionTV,dMouseCrossX,dMouseCrossY))!=-1)
            {
                interfaceTV->unActive();
                interfaceIR->unActive();
                interfaceTV->setActive(indeks);
                interfaceIR->setActive(indeks);
                isLeftPressed=true;
            } else if ((indeks=interfaceIR->checkPointPressed(positionIR,dMouseCrossX,dMouseCrossY))!=-1)
            {
                interfaceTV->unActive();
                interfaceIR->unActive();
                interfaceTV->setActive(indeks);
                interfaceIR->setActive(indeks);
                isLeftPressed=true;
            }
            else
            {
                interfaceTV->unActive();
                interfaceIR->unActive();
                isLeftPressed=false;
                interfaceTV->update_image();
                interfaceIR->update_image();
                imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
                ui->widget_kamery->showImage(imageloader->canvas);
            }
        }
    }
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else if(!interfaceTV)
        ui->label_komunikaty_l->setText("Brak interfejsu TV");
    else if(!interfaceIR)
        ui->label_komunikaty_l->setText("Brak interfejsu IR");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//obsługa ustawiania punktów na ekranie roboczym za pomocą myszki
void MainWindow::on_widget_kamery_signal_mouseLeftRelease(QMouseEvent *event)
{
    if(imageloader && interfaceTV && interfaceIR && calibration->point_saved>0)
    {
        if(czyWidgetKameryPokazujeFuzje==false)
        {
            QPoint positionTV = ui->widget_kamery->mapFromGlobal(event->globalPos());
            QPoint positionIR = ui->widget_kamery->mapFromGlobal(event->globalPos())-QPoint(IMAGE_WIDTH,0);

            positionTV+=pp;
            positionTV/=resizePar;
            positionIR+=pp;
            positionIR/=resizePar;

            int indeks=-1;
            for(unsigned int i=0;i<interfaceTV->points.size();i++)
                if(interfaceTV->points[i].active==true)
                    indeks=i;
            if(indeks!=-1)
            {
                if(positionTV.x()>=0 && positionTV.y()>=0 && positionTV.y()<IMAGE_HEIGHT/resizePar && positionTV.x()<IMAGE_WIDTH/resizePar)
                    isLeftPressed=false;
                if(positionIR.x()>=0 && positionIR.y()>=0 && positionIR.y()<IMAGE_HEIGHT/resizePar && positionIR.x()<IMAGE_WIDTH/resizePar)
                    isLeftPressed=false;
            }
            interfaceTV->update_image();
            interfaceIR->update_image();
            imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
            ui->widget_kamery->showImage(imageloader->canvas);
            float minAvDis = interfaceTV->minAverangeDistance();
            if(interfaceIR->minAverangeDistance()<minAvDis) minAvDis=interfaceIR->minAverangeDistance();
            ui->label_sredniaOdl->setText("Średnia odległość: "+QString::number(minAvDis));
            if(calibration->point_saved>=configuration->parameters[5] && minAvDis>=configuration->parameters[6])
            {
                calibration->calib(*interfaceTV, *interfaceIR);
                ui->widget_minifuzja->showImage(calibration->fusion_canvas);
            }
        }
    }
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else if(!interfaceTV)
        ui->label_komunikaty_l->setText("Brak interfejsu TV");
    else if(!interfaceIR)
        ui->label_komunikaty_l->setText("Brak interfejsu IR");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//obsługa ustawiania punktów na ekranie roboczym za pomocą myszki
void MainWindow::on_widget_kamery_signal_mouseLeftMove(QMouseEvent *event)
{
    if(imageloader && interfaceTV && interfaceIR && calibration->point_saved>0)
    {
        if(isLeftPressed==true && czyWidgetKameryPokazujeFuzje==false)
        {
            QPoint positionTV = ui->widget_kamery->mapFromGlobal(event->globalPos());
            QPoint positionIR = ui->widget_kamery->mapFromGlobal(event->globalPos())-QPoint(IMAGE_WIDTH,0);

            positionTV+=pp;
            positionTV/=resizePar;
            positionIR+=pp;
            positionIR/=resizePar;

            int indeks=-1;
            for(unsigned int i=0;i<interfaceTV->points.size();i++)
                if(interfaceTV->points[i].active==true)
                    indeks=i;
            if(indeks!=-1)
            {
                if(positionTV.x()>=pp.x()/resizePar && positionTV.y()>=pp.y()/resizePar && positionTV.x()-pp.x()/resizePar<IMAGE_WIDTH/resizePar && positionTV.y()-pp.y()/resizePar<IMAGE_HEIGHT/resizePar)
                    interfaceTV->setPoint(indeks,positionTV-QPoint(dMouseCrossX,dMouseCrossY));
                if(positionIR.x()>=pp.x()/resizePar && positionIR.y()>=pp.y()/resizePar && positionIR.x()-pp.x()/resizePar<IMAGE_WIDTH/resizePar && positionIR.y()-pp.y()/resizePar<IMAGE_HEIGHT/resizePar)
                    interfaceIR->setPoint(indeks,positionIR-QPoint(dMouseCrossX,dMouseCrossY));
            }
            interfaceTV->update_image();
            interfaceIR->update_image();
            imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
            ui->widget_kamery->showImage(imageloader->canvas);
        }
    }
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else if(!interfaceTV)
        ui->label_komunikaty_l->setText("Brak interfejsu TV");
    else if(!interfaceIR)
        ui->label_komunikaty_l->setText("Brak interfejsu IR");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//usuwanie pary aktywnych punktów
void MainWindow::on_pushButton_UsunAktywnePunkty_clicked()
{
    if(calibration && imageloader && interfaceTV && interfaceIR && czyWidgetKameryPokazujeFuzje==false)
    {
        int indeks=-1;
        for(unsigned int i=0;i<interfaceTV->points.size();i++)
            if(interfaceTV->points[i].active==true)
                indeks=i;
        if(indeks!=-1)
        {
            interfaceTV->deletePoint(indeks);
            interfaceIR->deletePoint(indeks);
            calibration->point_saved--;
            ui->label_Lparpunktow->setText("Liczba par punktów: "+QString::number(calibration->point_saved));
            interfaceTV->update_image();
            interfaceIR->update_image();
            imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
            ui->widget_kamery->showImage(imageloader->canvas);
        }
        else
            ui->label_komunikaty_l->setText("Wybierz punkty do usunięcia    ");
    }
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else if(!interfaceTV)
        ui->label_komunikaty_l->setText("Brak interfejsu TV");
    else if(!interfaceIR)
        ui->label_komunikaty_l->setText("Brak interfejsu IR");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//wyświetlenie fuzji na głównym ekranie roboczym
void MainWindow::on_pushButton_PokazFuzje_clicked()
{
    if(calibration && imageloader)
    {
        if(ui->pushButton_PokazFuzje->text()=="Pokaż fuzję")
        {
            czyWidgetKameryPokazujeFuzje=true;
            ui->groupBox_minifuzja->setTitle("Miniaturka kamer:");
            ui->widget_kamery->showImage(calibration->fusion_canvas);
            ui->widget_minifuzja->showImage(imageloader->canvas);
            ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
            ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");
            ui->pushButton_PokazFuzje->setText("Ukryj fuzję");
        }
        else
        {
            czyWidgetKameryPokazujeFuzje=false;
            ui->groupBox_minifuzja->setTitle("Miniaturka fuzji:");
            ui->widget_kamery->showImage(imageloader->canvas);
            ui->widget_minifuzja->showImage(calibration->fusion_canvas);
            ui->pushButton_PokazFuzje->setText("Pokaż fuzję");
        }
    }
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//wysłanie transformacji do platformy Jetson lub zapisanie na dysk
void MainWindow::on_pushButton_WyslanieTransformacji_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        if(ui->checkBox_reczna->checkState()==Qt::CheckState::Checked)
        {
            calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
            calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();
        }
        else
        {
            ui->lineEdit_skala_w->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(0,0),'g',4));
            ui->lineEdit_skala_h->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(1,1),'g',4));
        }

        int storage_status=storage.send_transformation(calibration->transformationMatrix_ir2tv,*configuration);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//resetowanie programu
void MainWindow::on_pushButton_Reset_clicked()
{
    QMessageBox msgBox(QMessageBox::Question,trUtf8("Reset"),"Czy chcesz zresetować program?",QMessageBox::Yes | QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, trUtf8("Tak"));
    msgBox.setButtonText(QMessageBox::No, trUtf8("Nie"));
    if(msgBox.exec() == QMessageBox::Yes)
    {
        if(configuration!=NULL) configuration=NULL;
        odczytanieParametrow();
        if(imageloader!=NULL)   imageloader=NULL;
        if(interfaceTV!=NULL)   interfaceTV=NULL;
        if(interfaceIR!=NULL)   interfaceIR=NULL;
        if(calibration!=NULL)   calibration=NULL;
        ui->widget_kamery->showImage(Mat(IMAGE_HEIGHT,IMAGE_WIDTH*2,CV_8UC3,Scalar(0,0,0)));
        ui->widget_minifuzja->showImage(Mat(IMAGE_HEIGHT/2,IMAGE_WIDTH/2,CV_8UC3,Scalar(0,0,0)));
        resizePar=1.0;
        pp=QPoint(0,0);
        ui->label_Lparpunktow->setText("Liczba par punktów: 0");
        ui->label_min_lparpkt->setText("Min liczba par punktów: "+QString::number(configuration->parameters[5]));
        ui->label_minSredniaOdl->setText("Min średnia odległość: "+QString::number(configuration->parameters[6]));
        ui->pushButton_PokazFuzje->setText("Pokaż fuzję");
        ui->groupBox_minifuzja->setTitle("Miniaturka fuzji:");
        ui->label_komunikaty_l->setText("Zresetowano program");
    }
}

//zmniejszenie minimalnej liczby punktów wymaganej do fuzji
void MainWindow::on_pushButton_min_number_of_points_pairs_down_clicked()
{
    if(configuration && configuration->parameters[5]>2)
    {
        configuration->parameters[5]--;
        ui->label_min_lparpkt->setText("Min liczba par punktów: "+QString::number(configuration->parameters[5]));
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!(configuration->parameters[5]>3))
        ui->label_komunikaty_l->setText("Min liczba par punktów nie może być mniejsza od 2");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//pokazanie fuzji na głównym ekranie roboczym po wciśnięciu miniaturki
void MainWindow::on_widget_minifuzja_signal_mouseLeftPress(QMouseEvent *)
{
    if(configuration && imageloader)
    {
        if(czyWidgetKameryPokazujeFuzje==false)
        {
            czyWidgetKameryPokazujeFuzje=true;
            ui->groupBox_minifuzja->setTitle("Miniaturka kamer:");
            ui->widget_kamery->showImage(calibration->fusion_canvas);
            ui->widget_minifuzja->showImage(imageloader->canvas);
            ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
            ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");
            ui->pushButton_PokazFuzje->setText("Ukryj fuzję");
        }
        else
        {
            czyWidgetKameryPokazujeFuzje=false;
            ui->groupBox_minifuzja->setTitle("Miniaturka fuzji:");
            ui->widget_kamery->showImage(imageloader->canvas);
            ui->widget_minifuzja->showImage(calibration->fusion_canvas);
            ui->pushButton_PokazFuzje->setText("Pokaż fuzję");
        }
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!(configuration->parameters[5]>3))
        ui->label_komunikaty_l->setText("Min liczba par punktów nie może być mniejsza od 3");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//przybliżenie obrazów
void MainWindow::on_pushButton_SizeUp_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        resizePar+=0.05;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//oddalenie obrazów
void MainWindow::on_pushButton_SizeDown_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        if(resizePar>1)
            resizePar-=0.05;
        if((pp.y()+IMAGE_HEIGHT)/resizePar>IMAGE_HEIGHT)
            pp.ry()=IMAGE_HEIGHT*resizePar-IMAGE_HEIGHT;
        if((pp.x()+IMAGE_WIDTH)/resizePar>IMAGE_WIDTH)
            pp.rx()=IMAGE_WIDTH*resizePar-IMAGE_WIDTH;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//obsługa pomoc - Whats This
void MainWindow::on_pushButton_Pomoc_clicked()
{
    QWhatsThis::enterWhatsThisMode();
}

//przesuwanie powiększonych obrazów
void MainWindow::on_pushButton_w_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        int speed = ui->lineEdit_speed->text().toInt();
        pp.ry()-=speed;
        if(pp.y()<0)
            pp.ry()=0;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//przesuwanie powiększonych obrazów
void MainWindow::on_pushButton_s_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        int speed = ui->lineEdit_speed->text().toInt();
        pp.ry()+=speed;
        if((pp.y()+IMAGE_HEIGHT)/resizePar>IMAGE_HEIGHT)
            pp.ry()=IMAGE_HEIGHT*resizePar-IMAGE_HEIGHT;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//przesuwanie powiększonych obrazów
void MainWindow::on_pushButton_a_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        int speed = ui->lineEdit_speed->text().toInt();
        pp.rx()-=speed;
        if(pp.x()<0)
            pp.rx()=0;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//przesuwanie powiększonych obrazów
void MainWindow::on_pushButton_d_clicked()
{
    if(configuration && imageloader && czyWidgetKameryPokazujeFuzje==false)
    {
        int speed = ui->lineEdit_speed->text().toInt();
        pp.rx()+=speed;
        if((pp.x()+IMAGE_WIDTH)/resizePar>IMAGE_WIDTH)
            pp.rx()=IMAGE_WIDTH*resizePar-IMAGE_WIDTH;
        imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
        ui->widget_kamery->showImage(imageloader->canvas);
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

//zmiana szybkości przesuwania powiększonego obrazu
void MainWindow::on_pushButton_speed_plus_clicked()
{
    int speed=ui->lineEdit_speed->text().toInt();
    ui->lineEdit_speed->setText(QString::number(speed+1));
}

//zmiana szybkości przesuwania powiększonego obrazu
void MainWindow::on_pushButton_speed_minus_clicked()
{
    int speed=ui->lineEdit_speed->text().toInt();
    if(speed>1)
        ui->lineEdit_speed->setText(QString::number(speed-1));
}

//zmniejszenie minimalnej średniej odległości między punktami
void MainWindow::on_pushButton_average_distance_between_points_down_clicked()
{
    if(configuration && configuration->parameters[6]>0)
    {
        configuration->parameters[6]--;
        ui->label_minSredniaOdl->setText("Min średnia odległość: "+QString::number(configuration->parameters[6]));
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!(configuration->parameters[6]>0))
        ui->label_komunikaty_l->setText("Min średnia odległość nie może być mniejsza od 0");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_checkBox_KalibracjaReczna_clicked()
{
    if(calibration && imageloader)
    {
        if(ui->pushButton_PokazFuzje->text()=="Pokaż fuzję")
        {
            ui->checkBox_KalibracjaReczna->setChecked(true);
            interfaceTV->points.clear();
            interfaceIR->points.clear();
            interfaceTV->addPoint();
            interfaceIR->addPoint();
            interfaceTV->addPoint();
            interfaceIR->addPoint();
            interfaceTV->addPoint();
            interfaceIR->addPoint();
            interfaceTV->addPoint();
            interfaceIR->addPoint();
            calibration->point_saved=4;
            interfaceIR->points.at(0).point.rx()+=80;
            interfaceIR->points.at(1).point.rx()+=80;
            interfaceIR->points.at(2).point.rx()-=80;
            interfaceIR->points.at(3).point.rx()-=80;
            interfaceIR->points.at(0).point.ry()+=80;
            interfaceIR->points.at(1).point.ry()-=80;
            interfaceIR->points.at(2).point.ry()+=80;
            interfaceIR->points.at(3).point.ry()-=80;
            interfaceIR->update_image();
            interfaceTV->points.at(0).point.rx()+=80;
            interfaceTV->points.at(1).point.rx()+=80;
            interfaceTV->points.at(2).point.rx()-=80;
            interfaceTV->points.at(3).point.rx()-=80;
            interfaceTV->points.at(0).point.ry()+=80;
            interfaceTV->points.at(1).point.ry()-=80;
            interfaceTV->points.at(2).point.ry()+=80;
            interfaceTV->points.at(3).point.ry()-=80;
            interfaceTV->update_image();
            imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);

            calibration->calib(*interfaceTV, *interfaceIR);

            ui->widget_kamery->showImage(imageloader->canvas);
            czyWidgetKameryPokazujeFuzje=true;
            ui->groupBox_minifuzja->setTitle("Miniaturka kamer:");
            ui->widget_kamery->showImage(calibration->fusion_canvas);
            ui->widget_minifuzja->showImage(imageloader->canvas);
            //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
            ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");
            ui->pushButton_PokazFuzje->setText("Ukryj fuzję");
        }
        else
        {
            czyWidgetKameryPokazujeFuzje=false;
            ui->groupBox_minifuzja->setTitle("Miniaturka fuzji:");
            ui->widget_kamery->showImage(imageloader->canvas);
            ui->widget_minifuzja->showImage(calibration->fusion_canvas);
            ui->pushButton_PokazFuzje->setText("Pokaż fuzję");
        }
    }
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_PrzesuniecieG_clicked()
{
    interfaceIR->points.at(0).point.ry()+=1;
    interfaceIR->points.at(1).point.ry()+=1;
    interfaceIR->points.at(2).point.ry()+=1;
    interfaceIR->points.at(3).point.ry()+=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_PrzesuniecieD_clicked()
{
    interfaceIR->points.at(0).point.ry()-=1;
    interfaceIR->points.at(1).point.ry()-=1;
    interfaceIR->points.at(2).point.ry()-=1;
    interfaceIR->points.at(3).point.ry()-=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_PrzesuniecieL_clicked()
{
    interfaceIR->points.at(0).point.rx()+=1;
    interfaceIR->points.at(1).point.rx()+=1;
    interfaceIR->points.at(2).point.rx()+=1;
    interfaceIR->points.at(3).point.rx()+=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_PrzesuniecieP_clicked()
{
    interfaceIR->points.at(0).point.rx()-=1;
    interfaceIR->points.at(1).point.rx()-=1;
    interfaceIR->points.at(2).point.rx()-=1;
    interfaceIR->points.at(3).point.rx()-=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_ObrotC_clicked()
{
    interfaceTV->points.at(0).point.rx()+=1;
    interfaceTV->points.at(0).point.ry()-=1;
    interfaceTV->points.at(1).point.rx()-=1;
    interfaceTV->points.at(1).point.ry()-=1;
    interfaceTV->points.at(3).point.rx()-=1;
    interfaceTV->points.at(3).point.ry()+=1;
    interfaceTV->points.at(2).point.rx()+=1;
    interfaceTV->points.at(2).point.ry()+=1;
    interfaceTV->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_ObrotCC_clicked()
{
    interfaceTV->points.at(0).point.rx()-=1;
    interfaceTV->points.at(0).point.ry()+=1;
    interfaceTV->points.at(1).point.rx()+=1;
    interfaceTV->points.at(1).point.ry()+=1;
    interfaceTV->points.at(3).point.rx()+=1;
    interfaceTV->points.at(3).point.ry()-=1;
    interfaceTV->points.at(2).point.rx()-=1;
    interfaceTV->points.at(2).point.ry()-=1;
    interfaceTV->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_Powieksz_clicked()
{
    interfaceIR->points.at(0).point.rx()-=1;
    interfaceIR->points.at(0).point.ry()-=1;
    interfaceIR->points.at(1).point.rx()-=1;
    interfaceIR->points.at(1).point.ry()+=1;
    interfaceIR->points.at(2).point.rx()+=1;
    interfaceIR->points.at(2).point.ry()-=1;
    interfaceIR->points.at(3).point.rx()+=1;
    interfaceIR->points.at(3).point.ry()+=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_Pomniejsz_clicked()
{
    interfaceIR->points.at(0).point.rx()+=1;
    interfaceIR->points.at(0).point.ry()+=1;
    interfaceIR->points.at(1).point.rx()+=1;
    interfaceIR->points.at(1).point.ry()-=1;
    interfaceIR->points.at(2).point.rx()-=1;
    interfaceIR->points.at(2).point.ry()+=1;
    interfaceIR->points.at(3).point.rx()-=1;
    interfaceIR->points.at(3).point.ry()-=1;
    interfaceIR->update_image();
    imageloader->update_canvas(interfaceTV->image_original_with_cross,interfaceIR->image_original_with_cross,resizePar,pp);
    calibration->calib(*interfaceTV, *interfaceIR);
    ui->widget_kamery->showImage(calibration->fusion_canvas);
    //ui->label_komunikaty_l->setText("Błąd kalibracji: "+QString::number(calibration->blad)+"; Kąt obrotu: "+QString::number(calibration->angle));
    ui->label_komunikaty_p->setText("Wektor translacji: ["+QString::number(calibration->tx)+","+QString::number(calibration->ty)+"]");

}

void MainWindow::on_pushButton_WyslanieMacierzy_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        if(ui->checkBox_reczna->checkState()==Qt::CheckState::Checked)
        {
            configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
            configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
            configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
            configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));
        }
        else
        {
            ui->lineEdit_k1_x->setText(QString::number(2.0*atan(512.0/(2.0*configuration->camera_matrix1.at<float>(0,0)))));
            ui->lineEdit_k1_y->setText(QString::number(2.0*atan(512.0/(2.0*configuration->camera_matrix1.at<float>(1,1)))));
            ui->lineEdit_k2_x->setText(QString::number(2.0*atan(512.0/(2.0*configuration->camera_matrix2.at<float>(0,0)))));
            ui->lineEdit_k2_y->setText(QString::number(2.0*atan(512.0/(2.0*configuration->camera_matrix2.at<float>(1,1)))));
        }

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_lineEdit_k1_x_returnPressed()
{
    if(configuration && imageloader && calibration)
    {
        configuration->angles[0] = ui->lineEdit_k1_x->text().toFloat();
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_lineEdit_k1_y_returnPressed()
{
    if(configuration && imageloader && calibration)
    {
        configuration->angles[1] = ui->lineEdit_k1_y->text().toFloat();
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_lineEdit_k2_y_returnPressed()
{
    if(configuration && imageloader && calibration)
    {
        configuration->angles[2] = ui->lineEdit_k2_x->text().toFloat();
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_lineEdit_k2_x_returnPressed()
{
    if(configuration && imageloader && calibration)
    {
        configuration->angles[3] = ui->lineEdit_k2_y->text().toFloat();
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_GenerujWspolczynnikLiniowy_clicked()
{
    if(configuration && imageloader && calibration)
    {
        cv::Size imgSize(512,512);
        std::vector<cv::Point2f> tv_points, ir_points;
        for(uint i = 0; i < interfaceTV->points.size(); i++)
        {
            tv_points.push_back(Point2f((float)interfaceTV->points.at(i).point.x()*512.0/640.0,(float)interfaceTV->points.at(i).point.y()*512.0/480.0));
            ir_points.push_back(Point2f((float)interfaceIR->points.at(i).point.x()*512.0/640.0,(float)interfaceIR->points.at(i).point.y()*512.0/480.0));
        }
        cv::Mat res = configuration->recalculateRectificationParameters(configuration->camera_matrix1, configuration->camera_matrix2, imgSize, tv_points, ir_points);

        if(ui->checkBox_reczna->checkState()==Qt::CheckState::Checked)
        {
            configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(configuration->angles[2]/2.0));
            configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(configuration->angles[3]/2.0));
            configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(configuration->angles[0]/2.0));
            configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(configuration->angles[1]/2.0));

            res.at<float>(0)=tan(ui->lineEdit_l_epi_a->text().toFloat());
            res.at<float>(1)=ui->lineEdit_l_epi_b->text().toFloat();
        }
        else
        {
            ui->lineEdit_l_epi_a->setText(QString::number(atan(res.at<float>(0)),'g',4));
            ui->lineEdit_l_epi_b->setText(QString::number(res.at<float>(1),'g',4));
        }

        cout<<res<<endl;
        Storage storage(*configuration,*imageloader);
        int storage_status=storage.send_epipolar(*configuration,res);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_WyslanieTransformacjiv2_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        if(ui->checkBox_reczna->checkState()==Qt::CheckState::Checked)
        {
            calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
            calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();
        }
        else
        {
            ui->lineEdit_skala_w->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(0,0),'g',4));
            ui->lineEdit_skala_h->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(1,1),'g',4));
        }

        int storage_status=storage.send_transformationv2(*configuration,calibration->transformationMatrix_ir2tv);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_Uaktualnij_katy_clicked()
{
    if(configuration && imageloader && calibration)
    {
        if(configuration->CameraMatrixSend)
        {
            configuration->angles[2]=2.0*atan(512.0/(2.0*configuration->camera_matrix2.at<float>(0,0)));
            configuration->angles[3]=2.0*atan(512.0/(2.0*configuration->camera_matrix2.at<float>(1,1)));
            configuration->angles[0]=2.0*atan(512.0/(2.0*configuration->camera_matrix1.at<float>(0,0)));
            configuration->angles[1]=2.0*atan(512.0/(2.0*configuration->camera_matrix1.at<float>(1,1)));
        }
        else
        {
            configuration->angles[2]=2.0*atan(512.0/(2.0*calibration->transformationMatrix_ir2tv.at<float>(Point(0,0))));
            configuration->angles[3]=2.0*atan(512.0/(2.0*calibration->transformationMatrix_ir2tv.at<float>(Point(1,1))));
            configuration->angles[0]=2.0*atan(521.0/2.0);
            configuration->angles[1]=2.0*atan(521.0/2.0);
        }
        ui->lineEdit_k1_x->setText(QString::number(configuration->angles[0]));
        ui->lineEdit_k1_y->setText(QString::number(configuration->angles[1]));
        ui->lineEdit_k2_x->setText(QString::number(configuration->angles[2]));
        ui->lineEdit_k2_y->setText(QString::number(configuration->angles[3]));

    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_checkBox_reczna_clicked()
{
    if(ui->checkBox_reczna->checkState()==Qt::CheckState::Checked)
    {
        ui->lineEdit_k1_x->setEnabled(true);
        ui->lineEdit_k1_y->setEnabled(true);
        ui->lineEdit_k2_x->setEnabled(true);
        ui->lineEdit_k2_y->setEnabled(true);
        ui->lineEdit_l_epi_a->setEnabled(true);
        ui->lineEdit_l_epi_b->setEnabled(true);
        ui->lineEdit_skala_h->setEnabled(true);
        ui->lineEdit_skala_w->setEnabled(true);

        ui->pushButton_tv_x_minus->setEnabled(true);
        ui->pushButton_tv_x_plus->setEnabled(true);
        ui->pushButton_tv_y_minus->setEnabled(true);
        ui->pushButton_tv_y_plus->setEnabled(true);

        ui->pushButton_ir_x_minus->setEnabled(true);
        ui->pushButton_ir_x_plus->setEnabled(true);
        ui->pushButton_ir_y_minus->setEnabled(true);
        ui->pushButton_ir_y_plus->setEnabled(true);

        ui->pushButton_lep_a_minus->setEnabled(true);
        ui->pushButton_lep_a_plus->setEnabled(true);
        ui->pushButton_lep_b_minus->setEnabled(true);
        ui->pushButton_lep_b_plus->setEnabled(true);

        ui->pushButton_skala_h_minus->setEnabled(true);
        ui->pushButton_skala_h_plus->setEnabled(true);
        ui->pushButton_skala_w_minus->setEnabled(true);
        ui->pushButton_skala_w_plus->setEnabled(true);
    }
    else
    {
        ui->lineEdit_k1_x->setEnabled(false);
        ui->lineEdit_k1_y->setEnabled(false);
        ui->lineEdit_k2_x->setEnabled(false);
        ui->lineEdit_k2_y->setEnabled(false);
        ui->lineEdit_l_epi_a->setEnabled(false);
        ui->lineEdit_l_epi_b->setEnabled(false);
        ui->lineEdit_skala_h->setEnabled(false);
        ui->lineEdit_skala_w->setEnabled(false);

        ui->pushButton_tv_x_minus->setEnabled(false);
        ui->pushButton_tv_x_plus->setEnabled(false);
        ui->pushButton_tv_y_minus->setEnabled(false);
        ui->pushButton_tv_y_plus->setEnabled(false);

        ui->pushButton_ir_x_minus->setEnabled(false);
        ui->pushButton_ir_x_plus->setEnabled(false);
        ui->pushButton_ir_y_minus->setEnabled(false);
        ui->pushButton_ir_y_plus->setEnabled(false);

        ui->pushButton_lep_a_minus->setEnabled(false);
        ui->pushButton_lep_a_plus->setEnabled(false);
        ui->pushButton_lep_b_minus->setEnabled(false);
        ui->pushButton_lep_b_plus->setEnabled(false);

        ui->pushButton_skala_h_minus->setEnabled(false);
        ui->pushButton_skala_h_plus->setEnabled(false);
        ui->pushButton_skala_w_minus->setEnabled(false);
        ui->pushButton_skala_w_plus->setEnabled(false);
    }
}

#define ZMIANA_KATOW 0.01

void MainWindow::on_pushButton_tv_x_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k1_x->setText(QString::number(ui->lineEdit_k1_x->text().toFloat()-ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_tv_x_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k1_x->setText(QString::number(ui->lineEdit_k1_x->text().toFloat()+ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_tv_y_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k1_y->setText(QString::number(ui->lineEdit_k1_y->text().toFloat()-ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_tv_y_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k1_y->setText(QString::number(ui->lineEdit_k1_y->text().toFloat()+ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_ir_x_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k2_x->setText(QString::number(ui->lineEdit_k2_x->text().toFloat()-ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_ir_x_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k2_x->setText(QString::number(ui->lineEdit_k2_x->text().toFloat()+ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_ir_y_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k2_y->setText(QString::number(ui->lineEdit_k2_y->text().toFloat()-ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_ir_y_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = true;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_k2_y->setText(QString::number(ui->lineEdit_k2_y->text().toFloat()+ZMIANA_KATOW));

        configuration->camera_matrix2.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k2_x->text().toFloat()/2.0));
        configuration->camera_matrix2.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k2_y->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(0,0)=512.0/(2.0*tan(ui->lineEdit_k1_x->text().toFloat()/2.0));
        configuration->camera_matrix1.at<float>(1,1)=512.0/(2.0*tan(ui->lineEdit_k1_y->text().toFloat()/2.0));

        cout<<configuration->camera_matrix1<<" "<<configuration->camera_matrix2<<endl;

        int storage_status=storage.send_matrix(*configuration,configuration->camera_matrix1,configuration->camera_matrix2);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_lep_a_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        cv::Size imgSize(512,512);
        std::vector<cv::Point2f> tv_points, ir_points;
        for(uint i = 0; i < interfaceTV->points.size(); i++)
        {
            tv_points.push_back(Point2f((float)interfaceTV->points.at(i).point.x()*512.0/640.0,(float)interfaceTV->points.at(i).point.y()*512.0/480.0));
            ir_points.push_back(Point2f((float)interfaceIR->points.at(i).point.x()*512.0/640.0,(float)interfaceIR->points.at(i).point.y()*512.0/480.0));
        }
        cv::Mat res = configuration->recalculateRectificationParameters(configuration->camera_matrix1, configuration->camera_matrix2, imgSize, tv_points, ir_points);

        ui->lineEdit_l_epi_a->setText(QString::number(ui->lineEdit_l_epi_a->text().toFloat()-0.5,'g',4));

        res.at<float>(0)=tan(ui->lineEdit_l_epi_a->text().toFloat());
        res.at<float>(1)=ui->lineEdit_l_epi_b->text().toFloat();

        cout<<res<<endl;
        Storage storage(*configuration,*imageloader);
        int storage_status=storage.send_epipolar(*configuration,res);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_lep_a_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        cv::Size imgSize(512,512);
        std::vector<cv::Point2f> tv_points, ir_points;
        for(uint i = 0; i < interfaceTV->points.size(); i++)
        {
            tv_points.push_back(Point2f((float)interfaceTV->points.at(i).point.x()*512.0/640.0,(float)interfaceTV->points.at(i).point.y()*512.0/480.0));
            ir_points.push_back(Point2f((float)interfaceIR->points.at(i).point.x()*512.0/640.0,(float)interfaceIR->points.at(i).point.y()*512.0/480.0));
        }
        cv::Mat res = configuration->recalculateRectificationParameters(configuration->camera_matrix1, configuration->camera_matrix2, imgSize, tv_points, ir_points);

        ui->lineEdit_l_epi_a->setText(QString::number(ui->lineEdit_l_epi_a->text().toFloat()+0.5,'g',4));

        res.at<float>(0)=tan(ui->lineEdit_l_epi_a->text().toFloat());
        res.at<float>(1)=ui->lineEdit_l_epi_b->text().toFloat();

        cout<<res<<endl;
        Storage storage(*configuration,*imageloader);
        int storage_status=storage.send_epipolar(*configuration,res);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_lep_b_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        cv::Size imgSize(512,512);
        std::vector<cv::Point2f> tv_points, ir_points;
        for(uint i = 0; i < interfaceTV->points.size(); i++)
        {
            tv_points.push_back(Point2f((float)interfaceTV->points.at(i).point.x()*512.0/640.0,(float)interfaceTV->points.at(i).point.y()*512.0/480.0));
            ir_points.push_back(Point2f((float)interfaceIR->points.at(i).point.x()*512.0/640.0,(float)interfaceIR->points.at(i).point.y()*512.0/480.0));
        }
        cv::Mat res = configuration->recalculateRectificationParameters(configuration->camera_matrix1, configuration->camera_matrix2, imgSize, tv_points, ir_points);

        ui->lineEdit_l_epi_b->setText(QString::number(ui->lineEdit_l_epi_b->text().toFloat()-1));

        res.at<float>(0)=ui->lineEdit_l_epi_a->text().toFloat();
        res.at<float>(1)=ui->lineEdit_l_epi_b->text().toFloat();

        cout<<res<<endl;
        Storage storage(*configuration,*imageloader);
        int storage_status=storage.send_epipolar(*configuration,res);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_lep_b_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        cv::Size imgSize(512,512);
        std::vector<cv::Point2f> tv_points, ir_points;
        for(uint i = 0; i < interfaceTV->points.size(); i++)
        {
            tv_points.push_back(Point2f((float)interfaceTV->points.at(i).point.x()*512.0/640.0,(float)interfaceTV->points.at(i).point.y()*512.0/480.0));
            ir_points.push_back(Point2f((float)interfaceIR->points.at(i).point.x()*512.0/640.0,(float)interfaceIR->points.at(i).point.y()*512.0/480.0));
        }
        cv::Mat res = configuration->recalculateRectificationParameters(configuration->camera_matrix1, configuration->camera_matrix2, imgSize, tv_points, ir_points);

        ui->lineEdit_l_epi_b->setText(QString::number(ui->lineEdit_l_epi_b->text().toFloat()+1));

        res.at<float>(0)=ui->lineEdit_l_epi_a->text().toFloat();
        res.at<float>(1)=ui->lineEdit_l_epi_b->text().toFloat();

        cout<<res<<endl;
        Storage storage(*configuration,*imageloader);
        int storage_status=storage.send_epipolar(*configuration,res);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_skala_w_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_skala_w->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(0,0)-ZMIANA_KATOW,'g',4));

        calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
        calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();

        cout<<calibration->transformationMatrix_ir2tv<<endl;

        int storage_status=storage.send_transformation(calibration->transformationMatrix_ir2tv,*configuration);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_skala_w_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_skala_w->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(0,0)+ZMIANA_KATOW,'g',4));

        calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
        calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();

        cout<<calibration->transformationMatrix_ir2tv<<endl;

        int storage_status=storage.send_transformation(calibration->transformationMatrix_ir2tv,*configuration);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_skala_h_minus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_skala_h->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(1,1)-ZMIANA_KATOW,'g',4));

        calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
        calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();

        cout<<calibration->transformationMatrix_ir2tv<<endl;

        int storage_status=storage.send_transformation(calibration->transformationMatrix_ir2tv,*configuration);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}

void MainWindow::on_pushButton_skala_h_plus_clicked()
{
    if(configuration && imageloader && calibration)
    {
        configuration->CameraMatrixSend = false;
        Storage storage(*configuration,*imageloader);

        ui->lineEdit_skala_h->setText(QString::number(calibration->transformationMatrix_ir2tv.at<float>(1,1)+ZMIANA_KATOW,'g',4));

        calibration->transformationMatrix_ir2tv.at<float>(0,0)=ui->lineEdit_skala_w->text().toFloat();
        calibration->transformationMatrix_ir2tv.at<float>(1,1)=ui->lineEdit_skala_h->text().toFloat();

        cout<<calibration->transformationMatrix_ir2tv<<endl;

        int storage_status=storage.send_transformation(calibration->transformationMatrix_ir2tv,*configuration);
        if(storage_status==0)
            ui->label_komunikaty_l->setText("Zapisywanie przekształcenia nie powiodło się.");
        else
            ui->label_komunikaty_l->setText("Zakończono zapisywanie wyznaczonego przekształcenia");
    }
    else if(!configuration)
        ui->label_komunikaty_l->setText("Brak konfiguracji");
    else if(!calibration)
        ui->label_komunikaty_l->setText("Brak kalibracji");
    else if(!imageloader)
        ui->label_komunikaty_l->setText("Nie wczytano obrazów");
    else
        ui->label_komunikaty_l->setText("Błąd");
}
