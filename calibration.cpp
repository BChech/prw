#include "calibration.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA WYKONANIE KALIBRACJI I WYŚWIETLENIE WYNIKÓW
//=====================================================================================

//konstruktor klasy
Calibration::Calibration(ImageLoader images,Configuration conf)
{
    images.tv.copyTo(tv_original);
    images.ir.copyTo(ir_original);

    crosses_projection_color=conf.colors[3];
    crosses_written_color=conf.colors[2];

    length=conf.parameters[3];
    inactiveThickness=conf.parameters[4];
    activeThickness=2*inactiveThickness;

    initialize_canvas(images.tv.cols,images.tv.rows);
}

//funkcja wykonanująca kalibrację i wyświetlająca jej wyniki
int Calibration::calib(ImageInterface intTV, ImageInterface intIR)
{
    tv_points.clear();
    ir_points.clear();

    for(unsigned int i=0;i<intTV.points.size();i++)
    {
        tv_points.push_back(intTV.points[i].point);
        ir_points.push_back(intIR.points[i].point);
    }

    int size=tv_points.size();

    cv::Mat irMat(3,size,CV_32F,Scalar(1));
    cv::Mat tvMat(2,size,CV_32F);

    for(int i=0;i<size;i++)
    {
        irMat.at<float>(0,i)=ir_points[i].x()*512.0/640.0;
        irMat.at<float>(1,i)=ir_points[i].y()*512.0/480.0;

        tvMat.at<float>(0,i)=tv_points[i].x()*512.0/640.0;
        tvMat.at<float>(1,i)=tv_points[i].y()*512.0/480.0;

    }

    transformationMatrix_ir2tv = tvMat*irMat.inv(CV_SVD);

    cv::Mat ir_at_tvMat(2,size,CV_32F);
    ir_at_tvMat=transformationMatrix_ir2tv*irMat;
    ir_at_tv_points.resize(0);
    for(int i=0;i<size;i++)
    {
         Point2f punkt(ir_at_tvMat.at<float>(0,i),ir_at_tvMat.at<float>(1,i));
         ir_at_tv_points.push_back(punkt);
    }

    tv_original.copyTo(tv);
    ir_original.copyTo(ir);

    for(int i=0;i<size;i++)
    {
        draw_cross(tv,tv_points[i],crosses_written_color,false);
        draw_cross(ir,ir_points[i],crosses_written_color,false);
        draw_cross(tv,Point2f(ir_at_tv_points[i].x*640.0/512.0,ir_at_tv_points[i].y*480.0/512.0),crosses_projection_color);
    }
    cv::warpAffine(ir, ir_transformed, transformationMatrix_ir2tv, ir.size());

    float suma=0;
    for(int i=0;i<size;i++)
    {
        float dx=ir_at_tv_points[i].x-tv_points[i].x();
        float dy=ir_at_tv_points[i].y-tv_points[i].y();
        float roznica=sqrt(dy*dy+dx*dx);
        suma=suma+roznica;
    }
    blad=suma/float(tv_points.size());
    if(blad<0.0001) blad=0;

    angle=57*atan2(transformationMatrix_ir2tv.at<float>(0,1),transformationMatrix_ir2tv.at<float>(0,0));
    if(angle<0.0001&&angle>-0.0001)angle=0;

    tx=transformationMatrix_ir2tv.at<float>(0,2);
    ty=transformationMatrix_ir2tv.at<float>(1,2);

    update_canvas(tv,ir_transformed);

    simple_fusion_canvas(tv,ir_transformed);
    update_canvas(tv,ir_transformed);
    return 0;
}

