#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "configuration.h"
#include "imageloader.h"
#include "operations.h"
#include "cv_image_widget.h"
#include "imageinterface.h"
#include "calibration.h"
#include "storage.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_WybierzPlik_clicked();

    void on_lineEdit_SciezkaPliku_returnPressed();

    void on_pushButton_PobranieObrazow_clicked();

    void on_pushButton_DodajPunkt_clicked();

    void on_widget_kamery_signal_mouseLeftPress(QMouseEvent *event);

    void on_widget_kamery_signal_mouseLeftRelease(QMouseEvent *event);

    void on_pushButton_PokazFuzje_clicked();

    void on_pushButton_UsunAktywnePunkty_clicked();

    void on_pushButton_WyslanieTransformacji_clicked();

    void on_pushButton_Reset_clicked();

    void on_pushButton_min_number_of_points_pairs_down_clicked();

    void on_widget_minifuzja_signal_mouseLeftPress(QMouseEvent *);

    void on_widget_kamery_signal_mouseLeftMove(QMouseEvent *event);

    void on_pushButton_SizeUp_clicked();

    void on_pushButton_SizeDown_clicked();

    void on_pushButton_Pomoc_clicked();

    void on_pushButton_w_clicked();

    void on_pushButton_s_clicked();

    void on_pushButton_a_clicked();

    void on_pushButton_d_clicked();

    void on_pushButton_speed_plus_clicked();

    void on_pushButton_speed_minus_clicked();

    void on_pushButton_average_distance_between_points_down_clicked();

    void on_checkBox_KalibracjaReczna_clicked();

    void on_pushButton_PrzesuniecieG_clicked();

    void on_pushButton_PrzesuniecieD_clicked();

    void on_pushButton_PrzesuniecieL_clicked();

    void on_pushButton_PrzesuniecieP_clicked();

    void on_pushButton_ObrotC_clicked();

    void on_pushButton_ObrotCC_clicked();

    void on_pushButton_Powieksz_clicked();

    void on_pushButton_Pomniejsz_clicked();

    void on_pushButton_WyslanieMacierzy_clicked();

    void on_lineEdit_k1_x_returnPressed();

    void on_lineEdit_k1_y_returnPressed();

    void on_lineEdit_k2_y_returnPressed();

    void on_lineEdit_k2_x_returnPressed();

    void on_pushButton_GenerujWspolczynnikLiniowy_clicked();

    void on_pushButton_WyslanieTransformacjiv2_clicked();

    void on_pushButton_Uaktualnij_katy_clicked();

    void on_checkBox_reczna_clicked();

    void on_pushButton_tv_x_minus_clicked();

    void on_pushButton_tv_x_plus_clicked();

    void on_pushButton_tv_y_minus_clicked();

    void on_pushButton_tv_y_plus_clicked();

    void on_pushButton_ir_x_minus_clicked();

    void on_pushButton_ir_x_plus_clicked();

    void on_pushButton_ir_y_minus_clicked();

    void on_pushButton_ir_y_plus_clicked();

    void on_pushButton_lep_a_minus_clicked();

    void on_pushButton_lep_a_plus_clicked();

    void on_pushButton_lep_b_minus_clicked();

    void on_pushButton_lep_b_plus_clicked();

    void on_pushButton_skala_w_minus_clicked();

    void on_pushButton_skala_w_plus_clicked();

    void on_pushButton_skala_h_minus_clicked();

    void on_pushButton_skala_h_plus_clicked();

private:
    Ui::MainWindow *ui;
    Configuration *configuration=NULL;
    ImageLoader *imageloader=NULL;
    ImageInterface *interfaceTV=NULL, *interfaceIR=NULL;
    Calibration *calibration=NULL;

    bool czyWidgetKameryPokazujeFuzje=false;    //czy główny ekran roboczy pokazuje fuzje
    bool isLeftPressed=false;                   //czy jest przesuwany krzyżyk
    int dMouseCrossX, dMouseCrossY;             //
    float resizePar;                            //stopień powiększenia
    QPoint pp;                                  //lewy górny wierzchołek wyświetlanych obrazów

    bool odczytanieParametrow();
    void setShortcuts();

};

#endif // MAINWINDOW_H
