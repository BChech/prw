#include "configuration.h"

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA ODCZYTANIE PLIKU KONFIGURACYJNEGO
//=====================================================================================

//konstruktor klasy - funkcja odczytująca parametry z pliku konfiguracyjnego
Configuration::Configuration(string calib_config_path)
{
    config_file_found=0;
    FileStorage file_reader;
    config_file_found=file_reader.open(calib_config_path, FileStorage::READ);

    if(config_file_found==1)
    {
        tcp_ip = (int)file_reader["tcp_ip"];
        file_reader["tv_path"] >> tv_path;
        file_reader["ir_path"] >> ir_path;
        file_reader["transformation_matrix_path"] >> transformation_matrix_path;

        file_reader["tcp_host_name"] >> tcp_host_name;
        tcp_port_number = (int)file_reader["tcp_port_number"];

        keys[0] = (int)file_reader["down"];
        keys[1] = (int)file_reader["left"];
        keys[2] = (int)file_reader["up"];
        keys[3] = (int)file_reader["right"];
        keys[4] = (int)file_reader["zoom_in"];
        keys[5] = (int)file_reader["zoom_out"];
        keys[6] = (int)file_reader["save_point"];
        keys[7] = (int)file_reader["change_IR/TV"];
        keys[8] = (int)file_reader["send_transformation"];
        keys[9] = (int)file_reader["calibration_improvement"];
        keys[10] = (int)file_reader["change_active_point"];
        keys[11] = (int)file_reader["delete_points_pair"];
        keys[12] = (int)file_reader["min_number_of_points_pairs_down"];
        keys[13] = (int)file_reader["average_distance_between_points_down"];
        keys[14] = (int)file_reader["delete_last_point"];
        keys[15] = (int)file_reader["simple_fusion_on_off"];
        keys[16] = (int)file_reader["reset_calibration"];
        keys[17] = (int)file_reader["start_calibration"];
        keys[18] = (int)file_reader["calibrate"];
        keys[19] = (int)file_reader["exit"];

        file_reader["tv_corss_color"] >> colors[0];
        file_reader["ir_corss_color"] >> colors[1];
        file_reader["written_corsses_color"] >> colors[2];
        file_reader["transformed_crosses_color"] >> colors[3];

        parameters[0] = (float)file_reader["zoom_resize"];
        parameters[1] = (float)file_reader["initial_cross_speed"];
        parameters[2] = (float)file_reader["cross_speed_reduction"];
        parameters[3] = (float)file_reader["cross_length"];
        parameters[4] = (float)file_reader["cross_width"];
        parameters[5] = (float)file_reader["min_points_number"];
        parameters[6] = (float)file_reader["min_average_distance"];
        parameters[7] = (float)file_reader["min_distance_for_saving"];
        parameters[8] = (float)file_reader["min_gradient"];

        angles[0] = (float)file_reader["k1x"];
        angles[1] = (float)file_reader["k1y"];
        angles[2] = (float)file_reader["k2x"];
        angles[3] = (float)file_reader["k2y"];

        rois[0] = (int)file_reader["k1left"];
        rois[1] = (int)file_reader["k1right"];
        rois[2] = (int)file_reader["k1up"];
        rois[3] = (int)file_reader["k1down"];
        rois[4] = (int)file_reader["k2left"];
        rois[5] = (int)file_reader["k2right"];
        rois[6] = (int)file_reader["k2up"];
        rois[7] = (int)file_reader["k2down"];
    }

    file_reader.release();

    cv::Size imgSizeTV(512-rois[0]-rois[1],512-rois[2]-rois[3]);
    cv::Size imgSizeIR(512-rois[4]-rois[5],512-rois[6]-rois[7]);
    calculateWfovCameraMatrices(camera_matrix1, camera_matrix2, imgSizeTV, imgSizeIR);
}

float Configuration::calcFocalLength(float field_of_view, int sensor_size)
{
    return sensor_size/(2 * tan(field_of_view/2));
}

void Configuration::calculateFocalLengths( float &tv_wfov_fx, float &tv_wfov_fy,
                            float &ir_wfov_fx, float &ir_wfov_fy,
                            int sensor_size_TV_w, int sensor_size_TV_h ,
                            int sensor_size_IR_w, int sensor_size_IR_h )
{

    float tv_wfov_x = angles[0];
    float tv_wfov_y = angles[1];
    float ir_wfov_x = angles[2];
    float ir_wfov_y = angles[3];

    tv_wfov_fx = calcFocalLength(tv_wfov_x, sensor_size_TV_w);
    tv_wfov_fy = calcFocalLength(tv_wfov_y, sensor_size_TV_h);
    ir_wfov_fx = calcFocalLength(ir_wfov_x, sensor_size_IR_w);
    ir_wfov_fy = calcFocalLength(ir_wfov_y, sensor_size_IR_h);
}

void Configuration::calculateWfovCameraMatrices(cv::Mat_<float> &camera_matrix_1, cv::Mat_<float> &camera_matrix_2, cv::Size image_sizeTV, cv::Size image_sizeIR)
{
    float tv_wfov_fx = 0.0, tv_wfov_fy = 0.0,
          ir_wfov_fx = 0.0, ir_wfov_fy = 0.0;
          //tv_nfov_fx, tv_nfov_fy
          //ir_nfov_fx, ir_nfov_fy;
    calculateFocalLengths( tv_wfov_fx, tv_wfov_fy,
                           ir_wfov_fx, ir_wfov_fy,
                           image_sizeTV.width, image_sizeTV.height,
                           image_sizeIR.width, image_sizeIR.height);
    camera_matrix_1 = Mat_<float>(3,3);
    camera_matrix_1[0][0] = tv_wfov_fx;
    camera_matrix_1[0][1] = (float)0;
    camera_matrix_1[0][2] = (float)(image_sizeTV.width/2);
    camera_matrix_1[1][0] = (float)0;
    camera_matrix_1[1][1] = tv_wfov_fy;
    camera_matrix_1[1][2] = (float)image_sizeTV.height/2;
    camera_matrix_1[2][0] = (float)0;
    camera_matrix_1[2][1] = (float)0;
    camera_matrix_1[2][2] = (float)1;

    camera_matrix_2 = Mat_<float>(3,3);
    camera_matrix_2[0][0] = ir_wfov_fx;
    camera_matrix_2[0][1] = (float)0;
    camera_matrix_2[0][2] = (float)(image_sizeIR.width/2);
    camera_matrix_2[1][0] = (float)0;
    camera_matrix_2[1][1] = ir_wfov_fy;
    camera_matrix_2[1][2] = (float)image_sizeIR.height/2;
    camera_matrix_2[2][0] = (float)0;
    camera_matrix_2[2][1] = (float)0;
    camera_matrix_2[2][2] = (float)1;
}

void Configuration::simpleStereoRectify(cv::Mat_<double> cameraMatrix1, cv::Mat_<double> cameraMatrix2, cv::Size image_size,
                         cv::Mat &R1, cv::Mat &R2, cv::Mat &P1, cv::Mat &P2)
{
    R1 = cv::Mat::eye(3, 3, CV_64F);
    R2 = cv::Mat::eye(3, 3, CV_64F);
    P1 = cv::Mat::zeros(3, 4, CV_64F);
    P2 = cv::Mat::zeros(3, 3, CV_64F);

    double fx1 = cameraMatrix1.at<double>(0, 0);
    double fy1 = cameraMatrix1.at<double>(1, 1);

    double fx2 = cameraMatrix2.at<double>(0, 0);
    double fy2 = cameraMatrix2.at<double>(1, 1);

    double max_fx = max(fx1, fx2);
    double max_fy = max(fy1, fy2);


    P1.at<double>(0, 0) = max_fx;
    P1.at<double>(1, 1) = max_fy;

    double cx1_prim = image_size.width/2;
    double cy1_prim = image_size.height/2;
    P1.at<double>(0, 2) = cx1_prim;
    P1.at<double>(1, 2) = cy1_prim;

    P1.at<double>(2, 2) = 1;//scale


    P2.at<double>(0, 0) = max_fx;
    P2.at<double>(1, 1) = max_fy;

    double cx2_prim = image_size.width/2;
    double cy2_prim = image_size.height/2;
    P2.at<double>(0, 2) = cx2_prim;
    P2.at<double>(1, 2) = cy2_prim;

    P2.at<double>(2, 2) = 1;//scale
}

cv::Mat Configuration::recalculateRectificationParameters(cv::Mat_<double> cameraMatrix1, cv::Mat_<double> cameraMatrix2, cv::Size image_size,
                                        std::vector< cv::Point2f > tv_points, std::vector< cv::Point2f > ir_points)
{
    std::vector< cv::Point2f > tv_points_prim, ir_points_prim;
    if(tv_points.size() == ir_points.size() && tv_points.size()>1)
    {
        cv::Mat R1, R2, P1, P2;
        simpleStereoRectify(cameraMatrix1, cameraMatrix2, image_size, R1, R2, P1, P2);

        rectifyPoints(tv_points, cameraMatrix1, R1, P1, tv_points_prim);
        rectifyPoints(ir_points, cameraMatrix2, R2, P2, ir_points_prim);
    }

    cv::Mat line_params;
    if(tv_points_prim.size() == ir_points_prim.size() && tv_points_prim.size()>1)
    {
        cv::Mat A(tv_points_prim.size(), 2, CV_32F);
        cv::Mat B(tv_points_prim.size(), 1, CV_32F);
        for(uint i = 0; i < tv_points_prim.size(); i++)
        {
            float u2_diff = ir_points_prim[i].x - tv_points_prim[i].x;
            float v2_diff = ir_points_prim[i].y - tv_points_prim[i].y;

            A.at<float>(i, 0) = v2_diff;
            A.at<float>(i, 1) = 1;
            B.at<float>(i, 0) = u2_diff;
        }
        cv::solve(A, B, line_params, DECOMP_SVD);
    }
    return line_params;
}

void Configuration::rectifyPoints(std::vector< cv::Point2f > src_points, cv::Mat_<double> cameraMatrix,/* cv::Size image_size,*/ cv::Mat_<double> R, cv::Mat_<double> P,
                   std::vector< cv::Point2f > &dst_points)
{
    double fx = cameraMatrix.at<double>(0, 0);
    double fy = cameraMatrix.at<double>(1, 1);
    double cx = cameraMatrix.at<double>(0, 2);
    double cy = cameraMatrix.at<double>(1, 2);

    double fx_prim = P.at<double>(0, 0);
    double fy_prim = P.at<double>(1, 1);
    double cx_prim = P.at<double>(0, 2);
    double cy_prim = P.at<double>(1, 2);

    dst_points.clear();
    for(uint i = 0; i < src_points.size(); i++)
    {
        double u = src_points[i].x;
        double v = src_points[i].y;

        double u_norm = (u - cx)/fx;
        double v_norm = (v - cy)/fy;

        cv::Mat_<double> uv1_norm(3, 1);
        uv1_norm << u_norm , v_norm, 1;

        cv::Mat_<double> point(3, 1);
        point = R.inv(DECOMP_LU) * uv1_norm;

        double u_prim_norm = point.at<double>(0, 0)/point.at<double>(2, 0);
        double v_prim_norm = point.at<double>(1, 0)/point.at<double>(2, 0);

        double u_prim = u_prim_norm*fx_prim + cx_prim;
        double v_prim = v_prim_norm*fy_prim + cy_prim;

        dst_points.push_back(Point2f(u_prim, v_prim));
    }
}
