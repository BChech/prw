#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <iostream>
#include <string.h>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#include "operations.h"
#include "configuration.h"

using namespace std;
using namespace cv;

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA WCZYTANIE OBRAZÓW
//=====================================================================================

class ImageLoader : public Operations
{
public:

    //konstruktor klasy
    ImageLoader(Configuration conf);

    Mat tv;Mat ir;  //obiekty przechowujące w pamięci odebrane obrazy



    string tv_path;string ir_path; //ścieżki pod którymi zapisane są obrazy na dysku
    string tcp_host_name;          //adres ip układu jetson
    int tcp_port_number;           //numer portu aplikacji działającej na układzie jetson

    //funkcja wczytująca obrazy i zapisujące je w pamięci
    int load_images(Configuration conf);
    //funkcja odtwarzająca obrazy na podstawie odebranego ciągu bajtów
    void make_images_from_message(uchar *tab);

    int message_size;//rozmiar odebranej wiadomości

    //wykkorzysytwane klawisze odczytane z pliku konfiguracyjnego
    char continue_key;
    char esc_key;

    int fov;//przekazywany kąt widzenia kamer


};

#endif // IMAGELOADER_H
