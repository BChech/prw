#-------------------------------------------------
#
# Project created by QtCreator 2016-12-28T13:15:24
#
#-------------------------------------------------


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KalibGuide
TEMPLATE = app

PKGCONFIG += opencv
CONFIG   -= app_bundle
CONFIG += c++11

INCLUDEPATH += /usr/local/include/opencv /usr/local/include/opencv2
LIBS += -L/usr/local/lib \
-lopencv_core \
-lopencv_calib3d \
-lopencv_video \
-lopencv_highgui \
-lopencv_imgproc
LIBS += -L/usr/local/lib -lboost_system

SOURCES += main.cpp configuration.cpp \
    operations.cpp \
    cv_image_widget.cpp \
    imageloader.cpp \
    imageinterface.cpp \
    calibration.cpp \
    storage.cpp \
    mainwindow.cpp

HEADERS  += configuration.h \
    operations.h \
    cv_image_widget.h \
    imageloader.h \
    imageinterface.h \
    calibration.h \
    storage.h \
    mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += resource.qrc
