#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/video.hpp"

using namespace std;
using namespace cv;

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA ODCZYTANIE PLIKU KONFIGURACYJNEGO
//=====================================================================================

class Configuration
{
public:

    //konstruktor klasy - funkcja odczytująca parametry z pliku konfiguracyjnego
    Configuration(string calib_config_path=NULL);

    int config_file_found;  //parametr określany na podstawie sukcesu odnalezienie pliku konfiguracyjnego

    int tcp_ip;                          //parametr określający użycie ethernetu w celu komunikacji
    string tv_path;string ir_path;       //ścieżki pod którymi zapisane są obrazy na dysku
    string transformation_matrix_path;   //ściezka pod którą ma zostać zapisana wyznaczona macierz

    string tcp_host_name;   //adres ip układu jetson
    int tcp_port_number;    //numer portu aplikacji działającej na układzie jetson

    char keys[20];          //używane klawisze z pliku konfiguracyjnego
    Scalar colors[4];       //uzywane kolory z pliku konfiguracyjnego
    float parameters[9];    //używane parametry  z pliku konfiguracyjnego
    float angles[4];          //kąty kamet TV i IR
    int rois[8];          //roi TV i IR

    float calcFocalLength(float field_of_view, int sensor_size);
    void calculateFocalLengths( float &tv_wfov_fx, float &tv_wfov_fy,
                                float &ir_wfov_fx, float &ir_wfov_fy,
                                int sensor_size_TV_w, int sensor_size_TV_h,
                                int sensor_size_IR_w, int sensor_size_IR_h);
    void calculateWfovCameraMatrices(cv::Mat_<float> &camera_matrix_1, cv::Mat_<float> &camera_matrix_2, cv::Size image_sizeTV, cv::Size image_sizeIR);

    void simpleStereoRectify(cv::Mat_<double> cameraMatrix1, cv::Mat_<double> cameraMatrix2, cv::Size image_size,
                             cv::Mat &R1, cv::Mat &R2, cv::Mat &P1, cv::Mat &P2);
    cv::Mat recalculateRectificationParameters(cv::Mat_<double> cameraMatrix1, cv::Mat_<double> cameraMatrix2, cv::Size image_size,
                                            std::vector< cv::Point2f > tv_points, std::vector< cv::Point2f > ir_points);
    void rectifyPoints(std::vector< cv::Point2f > src_points, cv::Mat_<double> cameraMatrix, cv::Mat_<double> R, cv::Mat_<double> P,
                       std::vector< cv::Point2f > &dst_points);
    cv::Mat_<float> camera_matrix1, camera_matrix2;
    bool CameraMatrixSend = false;

};

#endif // CONFIGURATION_H
