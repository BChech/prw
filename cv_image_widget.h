#ifndef CV_IMAGE_WIDGET_H
#define CV_IMAGE_WIDGET_H

#include <iostream>
#include <QWidget>
#include <QImage>
#include <QPainter>
#include <opencv2/opencv.hpp>
#include <QMouseEvent>

//=====================================================================================
// KLASA ODPOWIEDZIALNA ZA GŁÓWNY EKRAN ROBOCZY
//=====================================================================================

using namespace std;
using namespace cv;

class CVImageWidget : public QWidget
{
    Q_OBJECT

public:

    explicit CVImageWidget( QWidget *parent = 0 );
    QSize sizeHint() const;
    QSize minimumSizeHint() const;

public slots:

    void showImage( const cv::Mat& image, bool fixed_shape=true );

signals:
    void signal_mouseLeftPress(QMouseEvent *event);
    void signal_mouseRightPress(QMouseEvent *event);
    void signal_mouseLeftRelease(QMouseEvent *event);
    void signal_mouseRightRelease(QMouseEvent *event);
    void signal_mouseLeftMove(QMouseEvent *event);

protected:

    QImage _qimage;
    cv::Mat _input_image;

    void paintEvent(QPaintEvent* /*event*/);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

};

#endif
